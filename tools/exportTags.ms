if selection.count > 0 then (
	 output_name = getSaveFileName caption:"illRandomTags" types:"illRandomTags (*.txt)|*.txt|All Files (*.*)|*.*|"
	
	 if output_name != undefined then (
		output_file = createfile output_name
		
		--output number of objects
		format "%\n" selection.count to:output_file
		
		for i in selection do (
			
			yzSwap =  matrix3 [1,0,0] [0,0,-1] [0,1,0] [0,0,0]
			yzSwap2 =  matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]
			illTransform = i.objectTransform
				
			illTransform = yzSwap2 * illTransform * yzSwap
			
			--output name, omitting last 3 characters to get around Max duplicate name numbering, a slight hack
			--Be sure the object names always have some redundant 3 characters at the end, usually from being cloned
			format "%\n" (substring i.name 1 (i.name.count-3)) to:output_file
			
			--output 4x3 transform matrix in column major readable format
			format "% % % %\n" illTransform.row1[1] illTransform.row2[1] illTransform.row3[1] illTransform.row4[1] to:output_file
			format "% % % %\n" illTransform.row1[2] illTransform.row2[2] illTransform.row3[2] illTransform.row4[2] to:output_file
			format "% % % %\n" illTransform.row1[3] illTransform.row2[3] illTransform.row3[3] illTransform.row4[3] to:output_file			
		)
		
		close output_file
		edit output_name
	)
)
else (
	rollout thing "You have no objects selected. Only the selections are exported." ( )
)