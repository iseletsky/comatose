#ifndef ILL_STDIO_FILE_SYSTEM_H__
#define ILL_STDIO_FILE_SYSTEM_H__

#include "../FileSystem.h"

namespace illFileSystem {
class StdioFileSystem : public FileSystem {
public:
	~StdioFileSystem() {}
	
	virtual void addPath(const char * path) {}

	virtual bool fileExists(const char * path) const;

    virtual File * openRead(const char * path) const;
	virtual File * openWrite(const char * path) const;
    virtual File * openAppend(const char * path) const;
};
}

#endif
