#ifndef ILL_PHYSFS_FILE_SYSTEM_H__
#define ILL_PHYSFS_FILE_SYSTEM_H__

#include "../FileSystem.h"

namespace illFileSystem {
class PhysFsFileSystem : public FileSystem {
public:
    void init(const char * argv0);
    void deinit();

    virtual ~PhysFsFileSystem() {
        deinit();
    }

    void addPath(const char * path);

	virtual bool fileExists(const char * path) const;

    virtual File * openRead(const char * path) const;
	virtual File * openWrite(const char * path) const;
    virtual File * openAppend(const char * path) const;
};
}

#endif
