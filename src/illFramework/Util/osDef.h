/*
 * osDef.h
 *
 *  Created on: Dec 10, 2012
 *      Author: Ilya
 */

#ifndef ILL_OSDEF_H_
#define ILL_OSDEF_H_

//http://sourceforge.net/p/predef/wiki/OperatingSystems/

#define OS_WIN 1337
#define OS_LINUX OS_WIN + 1
#define OS_MAC OS_LINUX + 1
#define OS_ANDROID OS_MAC + 1
#define OS_IOS OS_ANDROID + 1
#define OS_INVALID 69696

#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined (__TOS_WIN__) || defined (__WINDOWS__)

#define CURR_OS OS_WIN

#elif defined(__APPLE__) && defined(__MACH__)

    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        #define CURR_OS OS_IOS
    #elif TARGET_IPHONE_SIMULATOR
        #define CURR_OS OS_IOS
    #elif TARGET_OS_MAC
        #define CURR_OS OS_MAC    
    #endif

#elif defined (__ANDROID__)

#define CURR_OS OS_ANDROID

#elif defined(__linux__)

#define OS_LINUX

#endif

//if it didn't get defined, FFFFUUUUUU
#ifndef CURR_OS
#error "Failed to determine which OS this is being compiled for."
#endif

#endif /* OSDEF_H_ */