#ifndef ILL_RESOURCEMANAGER_H__
#define ILL_RESOURCEMANAGER_H__

#include "../Logging/logging.h"
#include <string>
#include <map>
#include <vector>

#include "LruCache.h"
#include "Pool.h"

template<typename T, typename Loader>
class ResourceManager {
private:
    typedef LruCache<size_t, T> LruCacheType;

public:
    ResourceManager(Loader * loader)
        : m_loader(loader),

        m_resourceCache([this] (size_t id) {
            T * resource = new T();
            resource->load(id, this->m_loader);

            return resource;
        })
    {}

    ~ResourceManager() {}

    inline Loader * getLoader() const {
        return m_loader;
    }

    inline void setLoader(Loader * loader) {
        m_loader = loader;
    }

    inline RefCountPtr<T> getResource(size_t id) {
        return m_resourceCache.getElement(id);
    }

private:
    Loader * m_loader;
    LruCacheType m_resourceCache;
};

/**
*/
template<typename T, typename Loader>
class NamedResourceManager {
private:
    typedef LruCache<size_t, T> LruCacheType;

public:
    NamedResourceManager(Loader * loader)      
        : m_loader(loader),        

        m_resourceCache([this] (size_t id) {
            T * resource = new T();
            resource->load(this->m_namesArray[id], this->m_loader);

            return resource;
        })
    {}

    ~NamedResourceManager() {}

    inline Loader * getLoader() const {
        return m_loader;
    }

    inline void setLoader(Loader * loader) {
        m_loader = loader;
    }
    
    inline size_t getIdForName(const std::string& name) const {
        auto insertIter = m_nameMap.emplace(name, m_namesArray.size());

        if(insertIter.second) {
            m_namesArray.emplace_back(insertIter.first->first.c_str()); 
        }

        return insertIter.first->second;
    }

    inline RefCountPtr<T> getResource(size_t resourceId) {
        return m_resourceCache.getElement(resourceId);
    }

private:
    Loader * m_loader;
    LruCacheType m_resourceCache;
    mutable std::map<const std::string, size_t> m_nameMap;   
    mutable std::vector<const char *> m_namesArray;
};

#endif
