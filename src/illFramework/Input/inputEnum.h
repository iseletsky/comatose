#ifndef ILL_INPUT_ENUM_H__
#define ILL_INPUT_ENUM_H__

#include <cstdint>
#include <glm/glm.hpp>

//for the inputs
#include <GLFW/glfw3.h>

namespace illInput {
/**
Enumerates the axis types in a joystick or mouse
*/
enum class Axis {
    AX_VAL,     ///<The raw value, such as mouse position on screen

    AX_X,       ///<X axis
    AX_Y,       ///<Y axis

    AX_X_POS,   ///<X axis in the positive direction
    AX_X_NEG,   ///<X axis in the negative direction
    AX_Y_POS,   ///<Y axis in the positive direction
    AX_Y_NEG,   ///<Y axis in the negative direction
};

/**
A button index on a mouse or joystick
*/
typedef uint8_t Button;

/**
The values that go into the InputBinding device type.

SDL isn't very flexible at detecting multiple mice and keyboards,
so these are the hardcoded enums for now.

Normally I'd try to detect multiple devices and let them be bound per player.
*/
enum class InputType {
    INVALID,
    PC_KEYBOARD_TYPE,       ///<typing on the keyboard, so send all characters, modifiers, arrow keys, etc...
    PC_KEYBOARD,            ///<keys on the keyboard
    PC_MOUSE_BUTTON,        ///<buttons on the mouse
    PC_MOUSE_WHEEL,         ///<wheels on the mouse
    PC_MOUSE                ///<movement of the mouse
};

//TODO: figure out a place to keep all of the special input types
typedef glm::ivec2 MousePosition;

}

#endif