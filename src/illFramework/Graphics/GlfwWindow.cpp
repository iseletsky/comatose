#include "API/GlCommon/glInclude.h"
#include "API/GlCommon/glLogging.h"

#include "GlfwWindow.h"

#include "../Input/InputManager.h"
#include "../Input/InputBinding.h"
#include "../Input/inputEnum.h"


namespace illGraphics 
{
illInput::InputManager * GlfwWindow::inputManager = NULL;

GlfwWindow::~GlfwWindow() 
{
    uninitialize();
}

void GlfwWindow::initialize() {
    uninitialize();

    switch(m_glContextSetting) {
    case GlContext::GL_4_4:
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
        break;

    default:
        LOG_ERROR("Invalid OpenGL context specified.  Defaulting to OpenGL 3.3.");
        m_glContextSetting = GlContext::GL_3_3;

    case GlContext::GL_3_3:
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        break;
    }

    //TODO: for now
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    
    GLFWmonitor * monitor = NULL;

    switch(m_windowModeSetting) {
    case WindowMode::FULL:
        if(!m_monitorSetting.empty()) {
            int monitorsCount;        
            GLFWmonitor ** monitors = glfwGetMonitors(&monitorsCount);

            if(!monitors) {
                LOG_FATAL_ERROR("Error retreiving list of monitors");
            }

            //find the monitor by name, good thing I don't expect a computer to have many monitors attached
            for(int monitorInd = 0; monitorInd < monitorsCount; ++monitorInd) {
                if(strncmp(glfwGetMonitorName(monitors[monitorsCount]), m_monitorSetting.c_str(), m_monitorSetting.size()) == 0) {
                    monitor = monitors[monitorsCount];
                    break;
                }
            }

            if(!monitor) {
                monitor = glfwGetPrimaryMonitor();
                LOG_ERROR("Failed to detect monitor with name %s, falling back to default monitor", m_monitorSetting.c_str());
                m_monitorSetting = "";
            }
        }
        else {
            monitor = glfwGetPrimaryMonitor();
        }

        break;

    case WindowMode::BORDERLESS_WINDOWED:
        glfwWindowHint(GLFW_DECORATED, false);
        break;

    default:
        LOG_ERROR("Invalid windowing mode specified.  Defaulting to windowed.");

    case WindowMode::WINDOWED:
        glfwWindowHint(GLFW_DECORATED, true);
        break;
    }

    if(m_resizeableSetting) {
        glfwWindowHint(GLFW_RESIZABLE, true);
    }
    else {
        glfwWindowHint(GLFW_RESIZABLE, false);
    }

    m_glfwWindow = glfwCreateWindow(m_screenWidthSetting, m_screenHeightSetting, m_titleSetting.c_str(), monitor, 
        m_shareWindowSetting
            ? static_cast<GlfwWindow *>(m_shareWindowSetting)->m_glfwWindow
            : NULL);

    if(!m_glfwWindow) {
        LOG_FATAL_ERROR("Failed to initialize window");
    }

    glfwSetWindowUserPointer(m_glfwWindow, this);    

    //set up GLEW context
    glfwMakeContextCurrent(m_glfwWindow);
    m_glewContext = new GLEWContext();    
    glewContext = m_glewContext;

    ERROR_CHECK_OPENGL;

    glewExperimental = true;
    if(glewInit() != GLEW_OK) {
        LOG_FATAL_ERROR("Failed to initialize GLEW for the context being set up for this window");
    }

    //glewInit creates an invalid enum error for some reason, eat it up
    glGetError();

    ERROR_CHECK_OPENGL;

    {
        double x;
        double y;

        glfwGetCursorPos(m_glfwWindow, &x, &y);

        m_lastMousePos.x = (int) x;
        m_lastMousePos.y = (int) y;
    }
    
    glfwSetWindowSizeCallback(m_glfwWindow, windowResizeCallback);
    glfwSetCursorEnterCallback(m_glfwWindow, windowEnterCallback);
    glfwSetCursorPosCallback(m_glfwWindow, cursorPosCallback);
    glfwSetCharCallback(m_glfwWindow, charCallback);
    glfwSetKeyCallback(m_glfwWindow, keyCallback);
    glfwSetMouseButtonCallback(m_glfwWindow, mouseButtonCallback);
    glfwSetScrollCallback(m_glfwWindow, scrollCallback);

    m_screenWidth = m_screenWidthSetting;
    m_screenHeight = m_screenHeightSetting;
    m_title = m_titleSetting;
    m_windowMode = m_windowModeSetting;
    m_resizeable = m_resizeableSetting;
    m_aspectRatio = m_aspectRatioSetting == 0.0f 
        ? (m_screenHeight == 0 ? 1.0f : (float) m_screenWidth / m_screenHeight)
        : m_aspectRatioSetting;
    m_glContext = m_glContextSetting;

    m_state = m_windowMode == WindowMode::FULL 
        ? State::FULL 
        : State::WINDOWED;

    ERROR_CHECK_OPENGL;
}

void GlfwWindow::uninitialize() 
{
    if(m_state == State::INITIALIZING || m_state == State::RESIZING) {
        LOG_FATAL_ERROR("GlfwWindow::uninitialize() in unexpected state.");
    }

    if(m_state == State::UNINITIALIZED) {
        return;
    }
        
    glfwDestroyWindow(m_glfwWindow);
    delete m_glewContext;

    m_state = State::UNINITIALIZED;
}

void GlfwWindow::resize() {
    if(m_state == State::INITIALIZING || m_state == State::RESIZING) {
        LOG_FATAL_ERROR("GlfwWindow::resize() in unexpected state.");
    }

    //figure out if should completely just reset
    if((m_state != State::WINDOWED && (m_screenWidth != m_screenWidthSetting || m_screenHeight != m_screenHeightSetting))
        || m_windowModeSetting != m_windowMode
        || m_glContextSetting != m_glContext
        || m_resizeableSetting != m_resizeable) {
        initialize();
        return;
    }

    m_state = State::RESIZING;

    glfwSetWindowSize(m_glfwWindow, m_screenWidthSetting, m_screenHeightSetting);
    m_screenWidth = m_screenWidthSetting;
    m_screenHeight = m_screenHeightSetting;
    m_aspectRatio = m_aspectRatioSetting == 0.0f 
        ? (float) m_screenWidth / m_screenHeight 
        : m_aspectRatioSetting;

    m_state = State::WINDOWED;
}

void GlfwWindow::beginFrame() {
    glfwMakeContextCurrent(m_glfwWindow);
    glewContext = m_glewContext;
}

void GlfwWindow::endFrame() {
    glfwSwapBuffers(m_glfwWindow);
}

void GlfwWindow::setTitle(const char * title) {
    m_titleSetting = title;

    if(m_state == State::INITIALIZING || m_state == State::RESIZING) {
        LOG_FATAL_ERROR("GlfwWindow::setTitle() in unexpected state.");
    }

    if(m_state == State::UNINITIALIZED) {
        return;
    }

    glfwSetWindowTitle(m_glfwWindow, title);
    m_title = m_titleSetting;
}

void GlfwWindow::setCursorPos(const glm::ivec2& pos) {
    m_lastMousePos = pos;
    glfwSetCursorPos(m_glfwWindow, (double) pos.x, (double) pos.y);
}

void GlfwWindow::windowResizeCallback(GLFWwindow* window, int width, int height) {
    GlfwWindow * illWindow = static_cast<GlfwWindow *>(glfwGetWindowUserPointer(window));

    //Shouldn't crash to handle window being minimized and unminimized, those two things throw a resize event
    /*if(illWindow->getState() != State::WINDOWED) {
        LOG_FATAL_ERROR("Window was resized when it's not windowed");
    }*/

    illWindow->m_screenWidth = width;
    illWindow->m_screenHeight = height;
    illWindow->m_aspectRatio = illWindow->m_aspectRatioSetting == 0.0f 
        ? (height == 0 ? 1.0f : (float) illWindow->m_screenWidth / illWindow->m_screenHeight)
        : illWindow->m_aspectRatioSetting;

    //illWindow->resize();  //Not needed here or else infinite recursion will happen, resize is only called programmatically, I think?
}

void GlfwWindow::charCallback(GLFWwindow * window, unsigned int character) {
    //TODO?  If I need typing again
}

void GlfwWindow::cursorPosCallback(GLFWwindow * window, double x, double y) {
    GlfwWindow * illWindow = static_cast<GlfwWindow *>(glfwGetWindowUserPointer(window));

    illInput::InputBinding inputBinding;    
    inputBinding.m_deviceType = (int) illInput::InputType::PC_MOUSE;
    
    glm::ivec2 mousePos = glm::ivec2((int) x, (int) y);
    glm::ivec2 relMove = mousePos - illWindow->m_lastMousePos;    

    {
        inputBinding.m_input = (int)illInput::Axis::AX_VAL;
        illInput::MousePosition pos(mousePos);
        inputManager->onValueInput(inputBinding, &pos, sizeof(illInput::MousePosition));
    }

    if(relMove.x != 0) {
        inputBinding.m_input = (int)illInput::Axis::AX_X;
        inputManager->onAnalogInput(inputBinding, (float) relMove.x);

        if(relMove.x > 0) {
            inputBinding.m_input = (int)illInput::Axis::AX_X_POS;
            inputManager->onAnalogInput(inputBinding, (float) relMove.x);
        }

        if(relMove.x < 0) {
            inputBinding.m_input = (int)illInput::Axis::AX_X_NEG;
            inputManager->onAnalogInput(inputBinding, (float) -relMove.x);
        }
    }

    if(relMove.y != 0.0) {
        inputBinding.m_input = (int)illInput::Axis::AX_Y;
        inputManager->onAnalogInput(inputBinding, (float) relMove.y);

        if(relMove.y > 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_Y_POS;
            inputManager->onAnalogInput(inputBinding, (float) relMove.y);
        }
    
        if(relMove.y < 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_Y_NEG;
            inputManager->onAnalogInput(inputBinding, (float) -relMove.y);
        }
    }

    illWindow->m_lastMousePos = mousePos;
}

void GlfwWindow::mouseButtonCallback(GLFWwindow * window, int button, int action, int mods) {
    illInput::InputBinding inputBinding;
    
    inputBinding.m_deviceType = (int) illInput::InputType::PC_MOUSE_BUTTON;
    inputBinding.m_input = button;
            
    if(action == GLFW_PRESS) {
        inputManager->onPress(inputBinding);
    }
    else if(action == GLFW_RELEASE) {
        inputManager->onRelease(inputBinding);
    }
}

void GlfwWindow::keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    illInput::InputBinding inputBinding;
    
    inputBinding.m_deviceType = (int) illInput::InputType::PC_KEYBOARD;
    inputBinding.m_input = key;
            
    if(action == GLFW_PRESS) {
        inputManager->onPress(inputBinding);
    }
    else if(action == GLFW_RELEASE) {
        inputManager->onRelease(inputBinding);
    }
}

void GlfwWindow::scrollCallback(GLFWwindow * window, double x, double y) {
    illInput::InputBinding inputBinding;    
    inputBinding.m_deviceType = (int) illInput::InputType::PC_MOUSE_WHEEL;
        
    if(x != 0.0) {
        inputBinding.m_input = (int)illInput::Axis::AX_X;
        inputManager->onAnalogInput(inputBinding, (float) x);
        inputManager->onAnalogInput(inputBinding, 0.0f);

        if(x > 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_X_POS;
            inputManager->onAnalogInput(inputBinding, (float) x);
            inputManager->onAnalogInput(inputBinding, 0.0f);
        }

        if(x < 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_X_NEG;
            inputManager->onAnalogInput(inputBinding, (float) -x);
            inputManager->onAnalogInput(inputBinding, 0.0f);
        }
    }

    if(y != 0.0) {
        inputBinding.m_input = (int)illInput::Axis::AX_Y;
        inputManager->onAnalogInput(inputBinding, (float) y);
        inputManager->onAnalogInput(inputBinding, 0.0f);

        if(y > 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_Y_POS;
            inputManager->onAnalogInput(inputBinding, (float) y);
            inputManager->onAnalogInput(inputBinding, 0.0f);
        }
    
        if(y < 0.0) {
            inputBinding.m_input = (int)illInput::Axis::AX_Y_NEG;
            inputManager->onAnalogInput(inputBinding, (float) -y);
            inputManager->onAnalogInput(inputBinding, 0.0f);
        }
    }
}

void GlfwWindow::windowEnterCallback(GLFWwindow * window, int didEnter) {
    if(didEnter) {
        GlfwWindow * illWindow = static_cast<GlfwWindow *>(glfwGetWindowUserPointer(window));
        double x;
        double y;

        glfwGetCursorPos(window, &x, &y);
        illWindow->m_lastMousePos.x = (int) x;
        illWindow->m_lastMousePos.y = (int) y;
    }
}

}