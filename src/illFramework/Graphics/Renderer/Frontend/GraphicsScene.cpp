#include <cassert>

#include "../../../Math/Iterators/ConvexMeshIterator.h"

#include "GraphicsScene.h"
#include "LightNode.h"

#include "../Backend/RendererBackend.h"

#include "../../Objects/Camera/Camera.h"

namespace illGraphics {

void GraphicsScene::setupFrame() {
    uint64_t visibilityDuration = m_queryVisibilityDuration;
    uint64_t invisibilityDuration = m_queryInvisibilityDuration;

    if(m_numFramesOverflowed > 0) {
        visibilityDuration += m_queryVisibilityDurationGrowth * m_numFramesOverflowed;
        invisibilityDuration += m_queryInvisibilityDurationGrowth * m_numFramesOverflowed;
    }

    m_rendererBackend->retreiveCellQueries(m_queryFrames, m_frameCounter, visibilityDuration, invisibilityDuration, m_numFramesOverflowed * 2);
    m_rendererBackend->retreiveNodeQueries(m_frameCounter);
    m_rendererBackend->setupFrame();
   
    ++m_frameCounter;
}

void GraphicsScene::render(const illGraphics::Camera& camera, size_t viewport) {
    m_renderQueues.m_depthPassObjects = 0;
    m_rendererBackend->setupViewport(camera);

    //get the frustum iterator
    MeshEdgeList<> meshEdgeList = camera.getViewFrustum().getMeshEdgeList();
    MultiConvexMeshIterator<> frustumIterator;
    
    getGridVolume().orderedMeshIteratorForMesh(frustumIterator, &meshEdgeList,
        camera.getViewFrustum().m_nearTipPoint,
        camera.getViewFrustum().m_direction);
    
    bool needsQuerySetup = true;
        
    size_t numQueries = 0;
    size_t numQueriesNeeded = 0;
    
    //std::set<unsigned int> debugCellSet;
    
    bool recordedOverflow = false;

    while(!frustumIterator.atEnd()) {
        unsigned int currentCell = getGridVolume().indexForCell(frustumIterator.getCurrentPosition());

        /*if(debugCellSet.find(currentCell) != debugCellSet.end()) {
            LOG_ERROR("Cell %u traversed multiple times", currentCell);
        }

        debugCellSet.insert(currentCell);*/
                
        //check if cell is empty
        if(getSceneNodeCell(currentCell).empty() && getStaticNodeCell(currentCell).size() == 0) {
            frustumIterator.forward();
            continue;
        }

        ++numQueriesNeeded;

        //do an occlusion query for the cell
        void * cellQuery = NULL;
        
        bool visible = RendererBackend::decodeVisible(m_queryFrames.at(viewport)[currentCell]);
        uint64_t lastQueryFrame = RendererBackend::codeFrame(m_queryFrames.at(viewport)[currentCell]);
                
        //time to query
        /*if(m_performCull)*/ {
            if(lastQueryFrame <= m_frameCounter) {
                if(numQueries >= m_maxQueries) {  //check if queries overflowed
                    if(!recordedOverflow) {
                        ++m_numFramesOverflowed;
                        recordedOverflow = true;
                    }
                }

                if(numQueries <= m_maxQueries || visible) {  //if queries overflowed, force visible cells to requery to avoid blinking
                    ++numQueries;

                    if(needsQuerySetup) {
                        m_rendererBackend->setupQuery();
                        needsQuerySetup = false;
                    }

                    cellQuery = m_rendererBackend->occlusionQueryCell(
                        camera, vec3cast<unsigned int, glm::mediump_float>(frustumIterator.getCurrentPosition()) * getGridVolume().getCellDimensions() 
                            + getGridVolume().getCellDimensions() * 0.5f, 
                        getGridVolume().getCellDimensions(), currentCell, viewport);
                }
            }
        }
        
        frustumIterator.forward();

        //if cell was visible last frames and has objects in it
        if((visible && lastQueryFrame >= m_frameCounter)/* || !m_performCull*/) {
            //add all nodes in the cell to the render queues
            {
                auto& currCell = getSceneNodeCell(currentCell);

                for(auto cellIter = currCell.begin(); cellIter != currCell.end(); cellIter++) {
                    auto node = *cellIter;

                    if(node->addedToRenderQueue(m_renderAccessCounter)) {
                        continue;
                    }

                    //TODO: take this out after done with thesis                    
                    //node->setOcclusionCull(m_debugPerObjectCull);

                    if(node->getOcclusionCull() && node->getLastNonvisibleFrame(viewport) == m_frameCounter - 1) {
                        m_rendererBackend->occlusionQueryNode(camera, node, viewport);
                    }
                    else {
                        node->render(m_renderQueues);
                    }
                }
            }

            {
                auto& currCell = getStaticNodeCell(currentCell);

                for(size_t arrayInd = 0; arrayInd < currCell.size(); arrayInd++) {
                    auto node = currCell[arrayInd];

                    if(node->addedToRenderQueue(m_renderAccessCounter)) {
                        continue;
                    }

                    //TODO: take this out after done with thesis                    
                    //node->setOcclusionCull(m_debugPerObjectCull);

                    if(node->getOcclusionCull() && node->getLastNonvisibleFrame(viewport) == m_frameCounter - 1) {
                        m_rendererBackend->occlusionQueryNode(camera, node, viewport);
                    }
                    else {
                        node->render(m_renderQueues);
                    }
                }
            }

            m_rendererBackend->endQuery();
            needsQuerySetup = true;

            //draw objects for the depth pass
            m_rendererBackend->depthPass(m_renderQueues, camera, cellQuery, viewport);
        }
    }

    if(numQueries < m_maxQueries/** || !m_performCull*/) {        
        m_numFramesOverflowed = 0;
    }

    //LOG_DEBUG("Num Queries needed %u.  Countdown %u.  Num performed %u", numQueriesNeeded, m_resetRequeryDurationCountdown, numQueries);
    
    m_rendererBackend->render(m_renderQueues, camera, viewport);

    ++m_renderAccessCounter;

    m_renderQueues.m_depthPassSolidStaticMeshes.clear();
    m_renderQueues.m_lights.clear();
    m_renderQueues.m_solidStaticMeshes.clear();
    m_renderQueues.m_depthPassObjects = 0;
}

size_t GraphicsScene::registerViewport() {
    size_t res = m_returnViewportId++;
    Array<uint64_t>& framesArray = m_queryFrames[res];

    size_t numCells = getGridVolume().getCellNumber().x * getGridVolume().getCellNumber().y * getGridVolume().getCellNumber().z;

    framesArray.resize(numCells);
    memset(&framesArray[0], 0, sizeof(uint64_t) * numCells);

    return res;
}

void GraphicsScene::freeViewport(size_t viewport) {
    m_queryFrames.erase(viewport);
}

void GraphicsScene::getLights(const Box<>& boundingBox, std::set<LightNode*>& destination) const {    
    BoxIterator<> iter = m_interactionGrid.boxIterForWorldBounds(boundingBox);
    
    while(!iter.atEnd()) {
        //static lights
        {
            StaticLightNodeContainer& cell = m_staticLightNodes[m_grid.indexForCell(iter.getCurrentPosition())];

            for(size_t nodeInd = 0; nodeInd < cell.size(); nodeInd++) {
                LightNode * node = cell[nodeInd];
                assert(node->getType() == GraphicsNode::Type::LIGHT);

                if(node->m_accessCounter <= m_accessCounter) {
                    node->m_accessCounter = m_accessCounter + 1;

                    if(boundingBox.intersects(node->getWorldBoundingVolume())) {
                        destination.insert(node);
                    }
                }
            }
        }

        //moveable lights
        {
            LightNodeContainer& cell = m_lightNodes[m_grid.indexForCell(iter.getCurrentPosition())];

            for(auto nodeIter = cell.cbegin(); nodeIter != cell.cbegin(); nodeIter++) {
                LightNode * node = *nodeIter;
                assert(node->getType() == GraphicsNode::Type::LIGHT);

                if(node->m_accessCounter <= m_accessCounter) {
                    node->m_accessCounter = m_accessCounter + 1;

                    if(boundingBox.intersects(node->getWorldBoundingVolume())) {
                        destination.insert(node);
                    }
                }
            }
        }

        iter.forward();
    }

    ++m_accessCounter;
}

void GraphicsScene::addNode(GraphicsNode * node) {
    //regular nodes
    if(node->getType() != GraphicsNode::Type::LIGHT
            || (m_trackLightsInVisibilityGrid && node->getType() == GraphicsNode::Type::LIGHT)) {
        BoxIterator<> iter = m_grid.boxIterForWorldBounds(node->getWorldBoundingVolume());
       
        do {
            m_sceneNodes[m_grid.indexForCell(iter.getCurrentPosition())].insert(node);
        } while(iter.forward());
    }

    //lights
    if(node->getType() == GraphicsNode::Type::LIGHT) {
        BoxIterator<> iter = m_interactionGrid.boxIterForWorldBounds(node->getWorldBoundingVolume());
                
        do {
            m_lightNodes[m_grid.indexForCell(iter.getCurrentPosition())].insert(static_cast<LightNode *>(node));
        } while(iter.forward());
    }
}

void GraphicsScene::removeNode(GraphicsNode * node) {
    //regular nodes
    if(node->getType() != GraphicsNode::Type::LIGHT
            || (m_trackLightsInVisibilityGrid && node->getType() == GraphicsNode::Type::LIGHT)) {
        BoxIterator<> iter = m_grid.boxIterForWorldBounds(node->getWorldBoundingVolume());
            
        do {
            m_sceneNodes[m_grid.indexForCell(iter.getCurrentPosition())].erase(node);
        } while(iter.forward());
    }

    //lights
    if(node->getType() == GraphicsNode::Type::LIGHT) {
        BoxIterator<> iter = m_interactionGrid.boxIterForWorldBounds(node->getWorldBoundingVolume());
                
        do {
            m_lightNodes[m_grid.indexForCell(iter.getCurrentPosition())].erase(static_cast<LightNode *>(node));
        } while(iter.forward());
    }
}

void GraphicsScene::moveNode(GraphicsNode * node, const Box<>& prevBounds) {
    //remove
    {
        //regular nodes
        if(node->getType() != GraphicsNode::Type::LIGHT
                || (m_trackLightsInVisibilityGrid && node->getType() == GraphicsNode::Type::LIGHT)) {
            BoxOmitIterator<> iter = m_grid.boxOmitIterForWorldBounds(prevBounds, node->getWorldBoundingVolume());

            do {
                m_sceneNodes[m_grid.indexForCell(iter.getCurrentPosition())].insert(node);
            } while(iter.forward());
        }

        //lights
        if(node->getType() == GraphicsNode::Type::LIGHT) {
            BoxOmitIterator<> iter = m_interactionGrid.boxOmitIterForWorldBounds(prevBounds, node->getWorldBoundingVolume());

            do {
                m_lightNodes[m_grid.indexForCell(iter.getCurrentPosition())].insert(static_cast<LightNode *>(node));
            } while(iter.forward());
        }
    }
        
    //add
    {
        //regular nodes
        if(node->getType() != GraphicsNode::Type::LIGHT
                || (m_trackLightsInVisibilityGrid && node->getType() == GraphicsNode::Type::LIGHT)) {
            BoxOmitIterator<> iter = m_grid.boxOmitIterForWorldBounds(node->getWorldBoundingVolume(), prevBounds);

            do {
                m_sceneNodes[m_grid.indexForCell(iter.getCurrentPosition())].erase(node);
            } while(iter.forward());
        }

        //lights
        if(node->getType() == GraphicsNode::Type::LIGHT) {
            BoxOmitIterator<> iter = m_interactionGrid.boxOmitIterForWorldBounds(node->getWorldBoundingVolume(), prevBounds);

            do {
                m_lightNodes[m_grid.indexForCell(iter.getCurrentPosition())].erase(static_cast<LightNode *>(node));
            } while(iter.forward());
        }
    }
}

}