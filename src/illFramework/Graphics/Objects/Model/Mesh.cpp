#include "Mesh.h"
#include "../../Api/GraphicsBackend.h"

#include "../../../FileSystem/FileSystem.h"
#include "../../../FileSystem/File.h"

const uint64_t MESH_MAGIC = 0x494C4C4D45534831;	//ILLMESH1 in big endian 64 bit

namespace illGraphics {
void Mesh::unload() {
    if(m_state == RES_LOADING) {
        LOG_FATAL_ERROR("Attempting to unload mesh while it's loading");
    }

    if(m_state == RES_UNINITIALIZED || m_state == RES_UNLOADED) {
        return;
    }

    if(m_loader) {
        m_loader->unloadMesh(&m_meshBackendData);
        m_loader = NULL;
    }

    delete m_meshFrontendData;

    m_state = RES_UNLOADED;
}

void Mesh::setFrontentDataInternal(MeshData<> * mesh) {
    if(m_state == RES_LOADING) {
        LOG_FATAL_ERROR("Attempting to set mesh frontend while it's loading");
    }

    m_meshFrontendData = mesh;
}

void Mesh::frontendBackendTransferInternal(GraphicsBackend * loader, bool freeFrontendData) {
    m_loader = loader;
    m_loader->loadMesh(&m_meshBackendData, *m_meshFrontendData);

    if(freeFrontendData) {
        m_meshFrontendData->free();
    }

    m_state = RES_LOADED;
}

void Mesh::reload(GraphicsBackend * backend) {
    unload();

    illFileSystem::File * openFile = illFileSystem::fileSystem->openRead(m_loadArgs.c_str());
		
	//read magic string
	{
		uint64_t magic;
		openFile->readB64(magic);

		if(magic != MESH_MAGIC) {
			LOG_FATAL_ERROR("Not a valid ILLMESH1 file.");      //TODO: make this not fatal and instead load a crappy little box to indicate that the mesh failed to load
		}
	}

    FeaturesMask features;
    
    uint32_t numVert;
    uint16_t numInd;
    uint8_t numGroups;

    //read mesh features
	openFile->read8(features);

    //read the buffer sizes
    openFile->read8(numGroups);
	openFile->readL32(numVert);
	openFile->readL16(numInd);

    MeshData<> * mesh = new MeshData<>(numInd, numVert, numGroups, features);

    //read groups
    for(unsigned int group = 0; group < numGroups; group++) {
        MeshData<>::PrimitiveGroup& primitiveGroup = mesh->getPrimitiveGroup(group);

        {
            uint8_t data;
            openFile->read8(data);

            primitiveGroup.m_type = (MeshData<>::PrimitiveGroup::Type) data;
        }

        {
            uint16_t data;
            openFile->readL16(data);

            primitiveGroup.m_beginIndex = (uint32_t) data;
        }

        {
            uint16_t data;
            openFile->readL16(data);

            primitiveGroup.m_numIndices = (uint32_t) data;
        }
    }
                
    //TODO: format the binary data in the right endianness when releasing so it's safe to read the entire block in 1 go
    //read the VBO, some 1337 hax going on here
    size_t numElements = mesh->getVertexSize() / sizeof(float);

    for(unsigned int vertex = 0, vboIndex = 0; vertex < numVert; vertex++) {
        for(unsigned int elementIndex = 0; elementIndex < numElements; elementIndex++, vboIndex += sizeof(float)) {
			openFile->readLF(*reinterpret_cast<float *>(mesh->getData() + vboIndex));
        }
    }

    //read the IBO
    for(unsigned int index = 0, iboIndex = 0; index < numInd; index++, iboIndex++) {
        openFile->readL16(*(mesh->getIndices() + iboIndex));
    }

    setFrontentDataInternal(mesh);
    frontendBackendTransferInternal(backend, true);
}
}
