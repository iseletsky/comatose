#ifndef ILL_MESH_H__
#define ILL_MESH_H__

#include <string>
#include "../../../Util/ResourceBase.h"
#include "../../../Util/ResourceManager.h"
#include "../../../Math/MeshData.h"

namespace illGraphics {

class GraphicsBackend;

/**
Contains some mesh and its associated vertex buffer objects and index buffer objects.
*/
class Mesh : public ResourceBase<GraphicsBackend> {
public:
    Mesh()
        : m_meshFrontendData(NULL),
        m_meshBackendData(NULL)
    {}

    ~Mesh() {
        unload();
    }
    
    virtual void unload();
    virtual void reload(GraphicsBackend * backend);

    void setFrontentDataInternal(MeshData<> * mesh);
    void frontendBackendTransferInternal(GraphicsBackend * loader, bool freeFrontendData = true);

    inline void * getMeshBackendData() const {
        return m_meshBackendData;
    }

    inline MeshData<> * getMeshFrontentData() const {
        return m_meshFrontendData;
    }

private:
    MeshData<> * m_meshFrontendData;
    void * m_meshBackendData;
};

typedef NamedResourceManager<Mesh, GraphicsBackend> MeshManager;
}

#endif
