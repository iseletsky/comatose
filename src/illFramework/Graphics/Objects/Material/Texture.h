#ifndef ILL_TEXTURE_H__
#define ILL_TEXTURE_H__

#include <string>
#include "../../../Util/ResourceBase.h"
#include "../../../Util/ResourceManager.h"

namespace illGraphics {
    
class GraphicsBackend;

/**
Textures are used by materials for drawing 3D objects.
Sometimes a texture might be useable by itself outside of a material for drawing HUD elements or something.
*/
class Texture : public ResourceBase<GraphicsBackend> {
public:
    enum class Wrap {
        CLAMP_TO_EDGE,
        REPEAT
    };

    Texture()
        : ResourceBase(),
        m_textureData(NULL)
    {}

    virtual ~Texture() {
        unload();
    }

    virtual void unload();
    virtual void reload(GraphicsBackend * backend);

    inline void loadInternal(GraphicsBackend * backend, const char * path, Wrap wrapS, Wrap wrapT) {
        unload();

        m_wrapS = wrapS;
        m_wrapT = wrapT;
        m_loadArgs = path;

        reload(backend);
    }

    inline void* getTextureData() const {
        return m_textureData; 
    }

private:
    void* m_textureData;

    Wrap m_wrapS;         //wrap attribute value
    Wrap m_wrapT;         //wrap attribute value
};

typedef NamedResourceManager<Texture, GraphicsBackend> TextureManager;
}

#endif
