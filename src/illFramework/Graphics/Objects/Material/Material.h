#ifndef ILL_MATERIAL_H_
#define ILL_MATERIAL_H_

#include <glm/glm.hpp>

#include "../../../Util/ResourceBase.h"
#include "../../../Util/ResourceManager.h"
#include "ShaderProgram.h"
#include "Texture.h"

namespace illGraphics {

struct MaterialLoader {
    MaterialLoader(ShaderProgramManager * shaderProgramManager, TextureManager * textureManager)
        : m_shaderProgramManager(shaderProgramManager),
        m_textureManager(textureManager),
        m_forceForwardRendering(false)
    {}

    bool m_forceForwardRendering;
    ShaderProgramManager * m_shaderProgramManager;
    TextureManager * m_textureManager;
};

class Material : public ResourceBase<MaterialLoader> {
public:
    enum class BlendMode {
		NONE,                           ///<solid object, no blending
		ALPHA,                          ///<alpha blended
		PREMULT_ALPHA,                  ///<premultiplied alpha blended
		ADDITIVE                        ///<additively blended
    };

    enum class BillboardMode {
        NONE,                           ///<no billboarding
        XY,                             ///<the object is billboarded to face the camera in the x,y plane only
        XYZ                             ///<the object is fully billboarded to face the camera
    };

    //TODO: use this?
    enum class Reflectance {
        PHONG,
        SKIN
    };

	Material()
        : ResourceBase()
    {}

    virtual ~Material() {
        unload();
    }

    virtual void unload();
    virtual void reload(MaterialLoader * loader);

	inline const Texture * getDiffuseTexture() const {
        return m_diffuseTexture.get();
    }

    inline const Texture * getSpecularTexture() const {
        return m_specularTexture.get();
    }

    inline const Texture * getEmissiveTexture() const {
        return m_emissiveTexture.get();
    }

    inline const Texture * getNormalTexture() const {
        return m_normalTexture.get();
    }

    inline const ShaderProgram * getShaderProgram() const {
        return m_shaderProgram.get();
    }

    inline const ShaderProgram * getDepthPassProgram() const {
        return m_depthPassProgram.get();
    }

    inline const BlendMode getBlendMode() const {
        return m_blendMode;
    }

    inline const BillboardMode getBillboardMode() const {
        return m_billboardMode;
    }

    inline bool getNoLighting() const {
        return m_noLighting;
    }

    inline bool getSkinning() const {
        return m_skinning;
    }

    inline bool getForceForwardRendering() const {
        return m_forceForwardRendering;
    }
	
private:
    BlendMode m_blendMode;
    BillboardMode m_billboardMode;
    
    bool m_noLighting;
    bool m_skinning;
    bool m_forceForwardRendering;

    int m_diffuseTextureIndex;
    int m_specularTextureIndex;
    int m_emissiveTextureIndex;
    int m_normalTextureIndex;

	RefCountPtr<Texture> m_diffuseTexture;
	RefCountPtr<Texture> m_specularTexture;
	RefCountPtr<Texture> m_emissiveTexture;
	RefCountPtr<Texture> m_normalTexture;

    RefCountPtr<ShaderProgram> m_depthPassProgram;
	RefCountPtr<ShaderProgram> m_shaderProgram;
};

typedef NamedResourceManager<Material, MaterialLoader> MaterialManager;

}

#endif