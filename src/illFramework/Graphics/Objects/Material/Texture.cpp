#include "Texture.h"
#include "../../API/GraphicsBackend.h"

#include "../../../Logging/logging.h"

namespace illGraphics {

void Texture::unload() {
    if(m_state == RES_LOADING) {
        LOG_FATAL_ERROR("Attempting to unload texture while it's loading");
    }

    if(m_state == RES_UNINITIALIZED || m_state == RES_UNLOADED) {
        return;
    }

    m_loader->unloadTexture(&m_textureData);

    m_state = RES_UNLOADED;
}

void Texture::reload(GraphicsBackend * backend) {
    unload();

    m_loader = backend;

    m_state = RES_LOADING;

    //defaults
    const char * path = m_loadArgs.c_str();
    Wrap wrapS = Wrap::REPEAT;
    Wrap wrapT = Wrap::REPEAT;

    //check if this is a .illtxtr file
    /*if() {
        //TODO: parse .illtxtr file
    }*/

    //load texture with defaults
    m_loader->loadTexture(&m_textureData, path, wrapS, wrapT);

    m_state = RES_LOADED;
}

}
