#include <sstream>

#include "Material.h"
#include "../../API/GraphicsBackend.h"

#include "../../../Logging/logging.h"
#include "../../../FileSystem/FileSystem.h"
#include "../../../FileSystem/File.h"

namespace illGraphics {

void Material::unload() {
    if(m_state == RES_LOADING) {
        LOG_FATAL_ERROR("Attempting to unload material while it's loading");
    }

    if(m_state == RES_UNINITIALIZED || m_state == RES_UNLOADED) {
        return;
    }

	m_diffuseTexture.reset();
	m_specularTexture.reset();
	m_emissiveTexture.reset();
	m_normalTexture.reset();

	m_shaderProgram.reset();

    m_state = RES_UNLOADED;
}

void Material::reload(MaterialLoader * rendererBackend) {
    unload();

    m_loader = rendererBackend;

    m_state = RES_LOADING;

    uint64_t shaderMask = ShaderProgram::SHPRG_POSITIONS;
    uint64_t depthShaderMask = ShaderProgram::SHPRG_POSITIONS | ShaderProgram::SHPRG_FORWARD;

    //read file into string stream, not the most efficient method but whatever, material files are tiny right now
    //I might make them more complex with nodes and stuff later, and by then you'll need a tool and I'll just save materials as binary files
    illFileSystem::File * openFile = illFileSystem::fileSystem->openRead(m_loadArgs.c_str());
    size_t size = openFile->getSize();

    char * textBuffer = new char[size + 1];
    openFile->read(textBuffer, size);
    delete openFile;
    textBuffer[size] = 0;

    std::stringstream matStream(textBuffer);
    delete[] textBuffer;
        
    //init defaults
    m_diffuseTextureIndex = -1;
    m_emissiveTextureIndex = -1;
    m_specularTextureIndex = -1;
    m_normalTextureIndex = -1;
    m_billboardMode = BillboardMode::NONE;
    m_blendMode = BlendMode::NONE;
    m_forceForwardRendering = false;
    m_noLighting = false;
    m_skinning = false;

    //parse the mat file
    while(matStream.good()) {
        std::string token;
        matStream >> token;

        if(token.compare("diffuse") == 0) {
            if(m_diffuseTextureIndex != -1) {
                LOG_ERROR("Material file %s has a duplicate diffuse texture definition.", m_loadArgs.c_str());
            }
            
            matStream >> token;
            m_diffuseTextureIndex = m_loader->m_textureManager->getIdForName(token);
        }
        else if(token.compare("specular") == 0) {
            if(m_specularTextureIndex != -1) {
                LOG_ERROR("Material file %s has a duplicate specular texture definition.", m_loadArgs.c_str());
            }
            
            matStream >> token;
            m_specularTextureIndex = m_loader->m_textureManager->getIdForName(token);
        }
        else if(token.compare("emissive") == 0) {
            if(m_emissiveTextureIndex != -1) {
                LOG_ERROR("Material file %s has a duplicate emissive texture definition.", m_loadArgs.c_str());
            }
            
            matStream >> token;
            m_emissiveTextureIndex = m_loader->m_textureManager->getIdForName(token);
        }
        else if(token.compare("normal") == 0) {
            if(m_normalTextureIndex != -1) {
                LOG_ERROR("Material file %s has a duplicate emissive texture definition.", m_loadArgs.c_str());
            }
            
            matStream >> token;
            m_normalTextureIndex = m_loader->m_textureManager->getIdForName(token);
        }
        else if(token.compare("billboard") == 0) {
            matStream >> token;

            if(token.compare("none") == 0) {
                m_billboardMode = BillboardMode::NONE;
            }
            else if(token.compare("xy") == 0) {
                m_billboardMode = BillboardMode::XY;
            }
            else if(token.compare("xyz") == 0) {
                m_billboardMode = BillboardMode::XYZ;
            }
            else {
                LOG_ERROR("Invalid option %s specified for billboard mode in material file %s", token.c_str(), m_loadArgs.c_str());
            }
        }
        else if(token.compare("blend") == 0) {
            matStream >> token;

            if(token.compare("none") == 0) {
                m_blendMode = BlendMode::NONE;
            }
            else if(token.compare("additive") == 0) {
                m_blendMode = BlendMode::ADDITIVE;
            }
            else if(token.compare("alpha") == 0) {
                m_blendMode = BlendMode::ALPHA;
            }
            else if(token.compare("premultipliedAlpha") == 0) {
                m_blendMode = BlendMode::PREMULT_ALPHA;
            }
            else {
                LOG_ERROR("Invalid option %s specified for blend mode in material file %s", token.c_str(), m_loadArgs.c_str());
            }
        }
        else if(token.compare("forceForwardRendering") == 0) {
            m_forceForwardRendering = true;
        }
        else if(token.compare("noLighting") == 0) {
            m_noLighting = true;
        }
        else if(token.compare("skinning") == 0) {
            m_skinning = true;
        }
        else {
            LOG_ERROR("Invalid parameter %s specified for blend mode in material file %s", token.c_str(), m_loadArgs.c_str());
        }
    }

    //load textures
    bool normalsNeeded = false;

    //diffuse
    if(m_diffuseTextureIndex >= 0) {
        m_diffuseTexture = m_loader->m_textureManager->getResource(m_diffuseTextureIndex);
        shaderMask |= ShaderProgram::SHPRG_DIFFUSE_MAP;
        normalsNeeded = true;
    }
    
    //emissive
    if(m_emissiveTextureIndex >= 0) {
        m_emissiveTexture = m_loader->m_textureManager->getResource(m_emissiveTextureIndex);
        shaderMask |= ShaderProgram::SHPRG_EMISSIVE_MAP;
    }
    
    //specular
    if(m_specularTextureIndex >= 0 && !m_noLighting) {
        m_specularTexture = m_loader->m_textureManager->getResource(m_specularTextureIndex);
        shaderMask |= ShaderProgram::SHPRG_SPECULAR_MAP;
        normalsNeeded = true;
    }

    //normals
    if(m_normalTextureIndex >= 0 && !m_noLighting) {
        m_normalTexture = m_loader->m_textureManager->getResource(m_normalTextureIndex);
        shaderMask |= ShaderProgram::SHPRG_NORMAL_MAP;
        normalsNeeded = true;
    }
    
    //check if should do forward rendering instead of deferred shading
    if(m_loader->m_forceForwardRendering || m_forceForwardRendering || m_blendMode != BlendMode::NONE) {
        shaderMask |= ShaderProgram::SHPRG_FORWARD;

        if(!m_noLighting) {
            shaderMask |= ShaderProgram::SHPRG_FORWARD_LIGHT;
        }
    }
    else {
        normalsNeeded = true;   //normals are needed for deferred shading
    }

    //if any textures or lighting are used that may benefit from normals and no lighting isn't on
    if(normalsNeeded && !m_noLighting) {
        shaderMask |= ShaderProgram::SHPRG_NORMALS;
    }
        
    //TODO: billboarding mode

    //skinning
    if(m_skinning) {
        shaderMask |= ShaderProgram::SHPRG_SKINNING;
        depthShaderMask |= ShaderProgram::SHPRG_SKINNING;
    }

    //load render pass shader
    m_shaderProgram = m_loader->m_shaderProgramManager->getResource(shaderMask);

    //load depth pass shader
    if(m_blendMode == BlendMode::NONE) {
        m_depthPassProgram = m_loader->m_shaderProgramManager->getResource(depthShaderMask);
    }
    
    m_state = RES_LOADED;
}

}