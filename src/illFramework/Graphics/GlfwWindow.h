#ifndef ILL_GLFW_WINDOW_H_
#define ILL_GLFW_WINDOW_H_

#include <unordered_map>
#include "API/GlCommon/glInclude.h"
#include <GLFW/glfw3.h>
#include <functional>

namespace illInput {
class InputManager;
}

namespace illGraphics {

const float ASPECT_4_3 = 4.0f / 3.0f;
const float ASPECT_16_9 = 16.0f / 9.0f;
const float ASPECT_16_10 = 16.0f / 10.0f;

enum class WindowMode {
    FULL,
    WINDOWED,
    BORDERLESS_WINDOWED
};

const int DEFAULT_WIDTH = 640;
const int DEFAULT_HEIGHT = 480;
const WindowMode DEFAULT_WINDOW_MODE = WindowMode::WINDOWED;
const float DEFAULT_ASPECT_RATIO = 0.0f;

enum class GlContext {
    GL_3_3,
    GL_4_4
};

const GlContext DEFAULT_GL_CONTEXT = GlContext::GL_3_3;

typedef std::function<glm::ivec2(const glm::ivec2&)> CursorConstraintFunc;

class GlfwWindow {
public:
    enum class State {
        UNINITIALIZED,      ///<The window isn't ready for anything and must be initialized
        INITIALIZING,       ///<The window is currently initializing either for the first time or after a video mode change
        RESIZING,           ///<The window is currently resizing in windowed mode so a full restart isn't needed
        FULL,               ///<The window is fully ready for use in full screen mode
        WINDOWED            ///<The window is fully ready for use in windowed mode
    };

    GlfwWindow()
        : m_state(State::UNINITIALIZED),

        m_screenWidth(DEFAULT_WIDTH),
        m_screenWidthSetting(DEFAULT_WIDTH),
        
        m_screenHeight(DEFAULT_HEIGHT),
        m_screenHeightSetting(DEFAULT_HEIGHT),
                
        m_monitor(""),
        m_monitorSetting(""),
        
        m_windowMode(DEFAULT_WINDOW_MODE),
        m_windowModeSetting(DEFAULT_WINDOW_MODE),
        
        m_resizeable(false),
        m_resizeableSetting(false),

        m_aspectRatio(DEFAULT_ASPECT_RATIO),
        m_aspectRatioSetting(DEFAULT_ASPECT_RATIO),

        m_glContext(DEFAULT_GL_CONTEXT),
        m_glContextSetting(DEFAULT_GL_CONTEXT),

        m_shareWindow(NULL),
        m_shareWindowSetting(NULL)
    {}

    ~GlfwWindow();
    
    /**
    Initializes a window.
    */
    void initialize();

    /**
    Completely uninitializes a window.
    */
    void uninitialize();

    /**
    Call this when changing resolution without necessarily calling initialize().
    On Windows, resizing a window wouldn't require an initialize call.
    If in full screen or switching to full screen or out of fullscreen, any screen changes require a full reinitializing.
    */
    void resize();
    
    /**
    Called at the beginning of a frame
    */
    void beginFrame();

    /**
    Called at the end of a frame 
    */
    void endFrame();

    /**
    Returns the current state of the window.
    */
    inline State getState() const {
        return m_state;
    }

    inline int getScreenWidth() const {
        return m_screenWidth;
    }

    inline void setScreenWidth(int screenWidth) {
        m_screenWidthSetting = screenWidth;
    }

    inline int getScreenHeight() const {
        return m_screenHeight;
    }

    inline void setScreenHeight(int screenHeight) {
        m_screenHeightSetting = screenHeight;
    }
    
    inline const char * getMonitor() const {
        return m_monitor.c_str();
    }

    inline void setMonitor(const char * monitor) {
        m_monitorSetting = monitor;
    }

    inline WindowMode getWindowMode() const {
        return m_windowMode;
    }

    inline void setWindowMode(WindowMode windowMode) {
        m_windowModeSetting = windowMode;
    }

    inline bool getResizeable() const {
        return m_resizeable;
    }

    inline void setResizeable(bool resizeable) {
        m_resizeableSetting = resizeable;
    }

    inline float getAspectRatio() const {
        return m_aspectRatio;
    }

    inline void setApsectRatio(float aspectRatio) {
        m_aspectRatioSetting = aspectRatio;

        m_aspectRatio = m_aspectRatioSetting == 0.0f 
            ? (m_screenHeight == 0 ? 1.0f : (float) m_screenWidth / m_screenHeight)
            : m_aspectRatioSetting;
    }
    
    void setTitle(const char * title);
    
    inline const char * getTitle() const {
        return m_title.c_str();
    }

    inline void setGlContext(GlContext glContext) {
        m_glContext = glContext;
    }

    inline GlContext getGlContext() const {
        return m_glContext;
    }

    inline void setShareWindow(GlfwWindow * shareWindow) {
        m_shareWindow = shareWindow;
    }

    inline GlfwWindow * getShareWindow() const {
        return m_shareWindow;
    }

    inline GLFWwindow * getGlfwWindow() const {
        return m_glfwWindow;
    }
    
    void setCursorPos(const glm::ivec2& pos);
    
    static void windowResizeCallback(GLFWwindow * window, int width, int height);

    //TODO: this is how I'm doing input for now, maybe think of a better way to do this later
    static void charCallback(GLFWwindow * window, unsigned int character);
    static void cursorPosCallback(GLFWwindow * window, double x, double y);
    static void mouseButtonCallback(GLFWwindow * window, int button, int action, int mods);
    static void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
    static void scrollCallback(GLFWwindow * window, double x, double y);
    static void windowEnterCallback(GLFWwindow * window, int didEnter);

    //a bit weird, the input callbacks notify these input managers, I need a better way to do this...
    static illInput::InputManager * inputManager;
    
private:
    glm::ivec2 m_lastMousePos;

    GLFWwindow * m_glfwWindow;
    GLEWContext * m_glewContext;

    State m_state;
        
    int m_screenWidth;
    int m_screenWidthSetting;

    int m_screenHeight;
    int m_screenHeightSetting;
    
    std::string m_title;
    std::string m_titleSetting;

    std::string m_monitor;
    std::string m_monitorSetting;
    
    WindowMode m_windowMode;
    WindowMode m_windowModeSetting;

    bool m_resizeable;
    bool m_resizeableSetting;

    float m_aspectRatio;
    float m_aspectRatioSetting;

    //TODO: move this GL Context stuff outta here eventually to be more Graphics API agnostic
    GlContext m_glContext;
    GlContext m_glContextSetting;

    GlfwWindow * m_shareWindow;
    GlfwWindow * m_shareWindowSetting;
};

}

#endif