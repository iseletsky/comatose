#include "glInclude.h"
#include "GlBackend.h"
#include "glLogging.h"
#include "../../../Math/MeshData.h"

namespace illGraphics {

void GlBackend::loadMesh(void** meshBackendData, const MeshData<>& meshFrontendData) {
    *meshBackendData = new GLuint[2];
    
    glGenBuffers(2, (GLuint *)(*meshBackendData));
   
    glBindBuffer(GL_ARRAY_BUFFER, *((GLuint *)(*meshBackendData) + 0));
    glBufferData(GL_ARRAY_BUFFER, meshFrontendData.getNumVert() * meshFrontendData.getVertexSize(), meshFrontendData.getData(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *((GLuint *)(*meshBackendData) + 1));
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, meshFrontendData.getNumInd() * sizeof(uint16_t), meshFrontendData.getIndices(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    ERROR_CHECK_OPENGL;
}

void GlBackend::unloadMesh(void** meshBackendData) {
    glDeleteBuffers(2, (GLuint *)(*meshBackendData));
    delete[] (GLuint *)(*meshBackendData);
    *meshBackendData = NULL;

    ERROR_CHECK_OPENGL;
}

}
