#include <cstring>

#include <IL/il.h>

#include "glInclude.h"

#include "GlBackend.h"
#include "../../../FileSystem/FileSystem.h"
#include "../../../FileSystem/File.h"

#include "glLogging.h"

inline GLenum getTextureWrap(illGraphics::Texture::Wrap wrap) {
    switch(wrap) {
    case illGraphics::Texture::Wrap::CLAMP_TO_EDGE:
        return GL_CLAMP_TO_EDGE;

    case illGraphics::Texture::Wrap::REPEAT:
        return GL_REPEAT;

    default:
        return 0;
    }
}


namespace illGraphics {

void GlBackend::loadTexture(void ** textureData, const char * path, Texture::Wrap wrapS, Texture::Wrap wrapT) {
    //////////////////////////////////
    //declare stuff
    char * textureMemBuffer;
    size_t textureMemBufferLength;
    ILuint ilTexture;
    ILubyte* imageData = NULL;

    ///////////////////////////////////////////
    //copy texture from archive to buffer    
    illFileSystem::File * openFile = illFileSystem::fileSystem->openRead(path);
    textureMemBufferLength = openFile->getSize();

    textureMemBuffer = new char[textureMemBufferLength];

    openFile->read(textureMemBuffer, textureMemBufferLength);

    delete openFile;

    /////////////////////////////////
    //load image with DevIL
    ilGenImages(1, &ilTexture);
    ilBindImage(ilTexture);

    //make origin of images be lower left corner at all times
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilEnable(IL_ORIGIN_SET);

    if(!ilLoadL(IL_TYPE_UNKNOWN, textureMemBuffer, (ILuint) textureMemBufferLength)) {
        LOG_FATAL_ERROR("Error loading image %s", path);
    }

    ///////////////////////////////////
    //load the texture into OpenGL
    if(!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE)) {
        LOG_FATAL_ERROR("Error converting image %s to raw data", path);
    }

    int width = ilGetInteger(IL_IMAGE_WIDTH);
    int height = ilGetInteger(IL_IMAGE_HEIGHT);
    int imageBPP = ilGetInteger(IL_IMAGE_BPP);
            
    imageData = ilGetData();

    /////////////////////////////
    //set texture attributes
    GLuint texture;

    glGenTextures(1, &texture);

    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, getTextureWrap(wrapS));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, getTextureWrap(wrapT));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);

    /////////////////////////////
    //create texture in OpenGL
    gluBuild2DMipmaps(GL_TEXTURE_2D, imageBPP, /*paddedWidth, paddedHeight,*/width, height, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, imageData);
        
    ERROR_CHECK_OPENGL;

    ilDeleteImages(1, &ilTexture);

    ///////////////////////////////////
    //clean up
    delete[] textureMemBuffer;

    *textureData = new GLuint;
    memcpy(*textureData, &texture, sizeof(GLuint));
}

void GlBackend::unloadTexture(void ** textureData) {
    glDeleteTextures(1, (GLuint *)(*textureData));
    delete (GLuint *) *textureData;
    *textureData = NULL;

    ERROR_CHECK_OPENGL;
}

void initFont(void ** fontData, void ** charData, unsigned int charCount) {
}

void setFontData(void * fontData, void * charData, float left, float top, float width, float height) {
}

void unloadFont(void ** fontData, void ** charData, unsigned int charCount) {
}

}
