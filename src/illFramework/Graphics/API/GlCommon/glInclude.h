#ifndef ILL_GL_INCLUDE_H_
#define ILL_GL_INCLUDE_H_

#include <GL/glew.h>

//pretty much avoid touching this exposed variable unless you need to set it
//it's done this way for performance reasons since EVERY gl* function call uses the glewGetContext function.
extern GLEWContext * glewContext;
inline GLEWContext * glewGetContext() {
    return glewContext;
}

#endif