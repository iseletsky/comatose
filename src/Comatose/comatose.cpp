#include "comatose.h"
#include "Engine.h"

#include "FixedStepController.h"
#include "Game/MainController.h"

#include "../illFramework/Logging/SerialLogger.h"
#include "../illFramework/Logging/logging.h"
#include "../illFramework/Logging/StdioLogger.h"

#include "../illFramework/Console/DeveloperConsole.h"
#include "../illFramework/Console/CommandManager.h"
#include "../illFramework/Console/VariableManager.h"

#include "../illFramework/Input/InputManager.h"
#include "../illFramework/Input/inputEnum.h"

#include "../illFramework/FileSystem/Physfs/PhysFsFileSystem.h"

#include "../illFramework/Graphics/GlfwWindow.h"
#include "../illFramework/Graphics/API/GlCommon/GlBackend.h"

#include "../illFramework/Graphics/Objects/Material/Texture.h"
#include "../illFramework/Graphics/Objects/Material/Shader.h"
#include "../illFramework/Graphics/Objects/Material/ShaderProgram.h"
#include "../illFramework/Graphics/Objects/Material/Material.h"
#include "../illFramework/Graphics/Objects/Model/Mesh.h"

#include "inputBinds.h"

illLogging::Logger * illLogging::logger;
illFileSystem::FileSystem * illFileSystem::fileSystem;

namespace Coma {

int comaMain(int argc, const char ** argv) {
    Engine engine;

    //init console
    engine.m_developerConsole = new illConsole::DeveloperConsole();
    engine.m_developerConsole->m_variableManager = new illConsole::VariableManager();
    engine.m_developerConsole->m_commandManager = new illConsole::CommandManager();

    //init loggers
    illLogging::logger = new illLogging::SerialLogger();    
    illLogging::logger->addLogDestination(engine.m_developerConsole);

    LOGGER_BEGIN_CATCH

    //init file system
    illFileSystem::fileSystem = new illFileSystem::PhysFsFileSystem();
    static_cast<illFileSystem::PhysFsFileSystem *>(illFileSystem::fileSystem)->init(argv[0]);
    illFileSystem::fileSystem->addPath("..\\..\\..\\assets");       //TODO: point at game pak

    // Init GLFW:
    if(!glfwInit()) {
        LOG_FATAL_ERROR("Failed to initialize GLFW");
    }

    //init window and graphics
    engine.m_inputManager = new illInput::InputManager();
    illGraphics::GlfwWindow::inputManager = engine.m_inputManager;
    engine.m_window = new illGraphics::GlfwWindow();    
    engine.m_graphicsBackend = new illGraphics::GlBackend();

    //init resource managers
    engine.m_shaderManager = new illGraphics::ShaderManager(engine.m_graphicsBackend);

    illGraphics::ShaderProgramLoader shaderProgramLoader(engine.m_graphicsBackend, engine.m_shaderManager);
    engine.m_shaderProgramManager = new illGraphics::ShaderProgramManager(&shaderProgramLoader);

    engine.m_textureManager = new illGraphics::TextureManager(engine.m_graphicsBackend);

    illGraphics::MaterialLoader materialLoader(engine.m_shaderProgramManager, engine.m_textureManager);
    engine.m_materialManager = new illGraphics::MaterialManager(&materialLoader);

    engine.m_meshManager = new illGraphics::MeshManager(engine.m_graphicsBackend);

    //init inputs
    engine.m_inputManager->addPlayer(0);
    engine.m_inputManager->bindDevice((int) illInput::InputType::PC_KEYBOARD, 0);
    engine.m_inputManager->bindDevice((int) illInput::InputType::PC_MOUSE, 0);
    engine.m_inputManager->bindDevice((int) illInput::InputType::PC_MOUSE_BUTTON, 0);
    engine.m_inputManager->bindDevice((int) illInput::InputType::PC_MOUSE_WHEEL, 0);
    
    //init cvars
    //The console variables and commands
    //They're done here so the things the lambdas reference stay in scope, it is a bit ugly having this massive function though...
    illConsole::ConsoleVariable cv_con_visible("0", 
        "Whether or not the console is visible or hidden.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);

            bool dest;
            if(engine.m_developerConsole->getParamBool(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                if(dest) {
                    //console.show();
                }
                else {
                    //console.hide();
                }
                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("con_visible", &cv_con_visible);

    illConsole::ConsoleVariable cv_con_output("", 
        "If set, the console will print all output to a file in real time.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            engine.m_developerConsole->setOutputFile(value);
            return true;
        });

    engine.m_developerConsole->m_variableManager->addVariable("con_output", &cv_con_output);

    illConsole::ConsoleVariable cv_con_maxLines("256", 
        "The maximum number of lines the console will remember at a time. "
        "It may not necessarily have room to display all of these lines, "
        "but dumping the console to a file with conDump will dump all of the lines that are remembered.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            int dest;
            if(engine.m_developerConsole->getParamInt(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_developerConsole->setMaxLines((size_t) dest);
                return true;
            }
        
            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("con_maxLines", &cv_con_maxLines);

    //TODO: implement
    illConsole::ConsoleVariable cv_con_logScreen("0", 
        "If set, all messages printed to the console will be displayed on the screen as well "
        "even when the console is closed.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "I still need to implement this.");
            return true;
        });

    engine.m_developerConsole->m_variableManager->addVariable("con_logScreen", &cv_con_logScreen);

    illConsole::ConsoleCommand cm_set(
        "Sets a console variable. "
        "set <variable name> <value>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string varName;
            std::string value;

            if(engine.m_developerConsole->getParamString(stream, varName)
                    && engine.m_developerConsole->getParamString(stream, value)
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                illConsole::ConsoleVariable * var = engine.m_developerConsole->m_variableManager->getVariable(varName.c_str());

                if(var) {
                    var->setValue(value.c_str());
                }
                else {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_INFO, formatString("Adding new console variable %s", varName.c_str()).c_str());
                    engine.m_developerConsole->m_variableManager->addVariable(varName.c_str(), new illConsole::ConsoleVariable(value.c_str(), "Newly created variable by setting a previously undefined variable name."));
                }
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("set", &cm_set);

    //TODO: implement
    illConsole::ConsoleCommand cm_save(
        "Saves a console variable permanently. "
        "save <variable name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "I still need to implement this.");
        });

    engine.m_developerConsole->m_commandManager->addCommand("save", &cm_save);

    illConsole::ConsoleCommand cm_description(
        "Prints the description of a variable or command. Inception bro. "
        "description <command or variable name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string name;

            if(engine.m_developerConsole->getParamString(stream, name)
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                illConsole::ConsoleVariable * var = engine.m_developerConsole->m_variableManager->getVariable(name.c_str());

                if(var) {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_INFO, var->getDescription());
                    return;
                }

                const illConsole::ConsoleCommand * cmd = engine.m_developerConsole->m_commandManager->getCommand(name.c_str());

                if(cmd) {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_INFO, cmd->getDescription());
                    return;
                }

                engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, formatString("No variable or command with name %s.", name.c_str()).c_str());
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("description", &cm_description);

    illConsole::ConsoleCommand cm_toggle(
        "The command for dumping all remembered lines in the developer console to a file. "
        "conDump <file name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string varName;

            if(engine.m_developerConsole->getParamString(stream, varName)
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                illConsole::ConsoleVariable * var = engine.m_developerConsole->m_variableManager->getVariable(varName.c_str());

                if(var) {
                    const char * value = var->getValue();

                    if(strncmp(value, "0", 1) == 0) {
                        var->setValue("1");
                    }
                    else {
                        var->setValue("0");
                    }
                }
                else {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, formatString("No variable with name %s.", varName.c_str()).c_str());
                }
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("toggle", &cm_toggle);

    illConsole::ConsoleCommand cm_conDump(
        "The command for dumping all remembered lines in the developer console to a file. "
        "conDump <file name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string path;

            if(engine.m_developerConsole->getParamString(stream, path)
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_developerConsole->consoleDump(path.c_str());
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("conDump", &cm_conDump);

    illConsole::ConsoleCommand cm_exec(
        "Executes the commands in a file. "
        "exec <file name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string path;

            if(engine.m_developerConsole->getParamString(stream, path)
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_developerConsole->consoleInput(path.c_str());
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("exec", &cm_exec);

    illConsole::ConsoleCommand cm_clear(
        "Clears all lines in the developer console.",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            if(engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_developerConsole->clearLines();
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("clear", &cm_clear);

    illConsole::ConsoleCommand cm_echo(
        "Prints a message to the console. echo <some message>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            //TODO: remove the white space that occurs before the text to echo
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MessageLevel::MT_INFO, params);
        });

    engine.m_developerConsole->m_commandManager->addCommand("echo", &cm_echo);

    illConsole::ConsoleVariable cv_vid_showFps("0", 
        "Shows a frames per second graph if enabled.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            bool dest;
            if(engine.m_developerConsole->getParamBool(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {            
                //engine.m_showingFps = dest;
                        //TODO

                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_showFps", &cv_vid_showFps);

    illConsole::ConsoleVariable cv_vid_screenWidth("640", 
        "Screen resolution width in pixels. Call vid_applyResolution to make changes take place after setting.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            int dest;
            if(engine.m_developerConsole->getParamInt(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_window->setScreenWidth(dest);
                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_screenWidth", &cv_vid_screenWidth);

    illConsole::ConsoleVariable cv_vid_screenHeight("480", 
        "Screen resolution height in pixels. Call vid_applyResolution to make changes take place after setting.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            int dest;
            if(engine.m_developerConsole->getParamInt(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_window->setScreenHeight(dest);
                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_screenHeight", &cv_vid_screenHeight);
    
    illConsole::ConsoleVariable cv_vid_windowMode("0", 
        "The windowing mode for the application. "
        "windowed to make it be windowed. "
        "borderless to make it be windowed but borderless. "
        "full to make it be full screen. ",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            std::string dest;
            if(engine.m_developerConsole->getParamString(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                if(dest == "windowed") {
                    engine.m_window->setWindowMode(illGraphics::WindowMode::WINDOWED);
                }
                else if(dest == "borderless") {
                    engine.m_window->setWindowMode(illGraphics::WindowMode::BORDERLESS_WINDOWED);
                }
                else if(dest == "full") {
                    engine.m_window->setWindowMode(illGraphics::WindowMode::FULL);
                }
                else {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "Expecting either windowed, borderless, or full.");
                    return false;
                }

                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_windowMode", &cv_vid_windowMode);

    //TODO: implement
    illConsole::ConsoleVariable cv_vid_monitor("", 
        "Which monitor name will the application run in full screen on. "
        "Doesn't apply to windowed mode. "
        "Leave blank to use the primary monitor. "
        "Call vid_printMonitors to print out monitor names. "
        "Call vid_applyResolution to make changes take place after setting.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "TODO: implement.");
            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_monitor", &cv_vid_monitor);

    illConsole::ConsoleVariable cv_vid_aspect("0", 
        "The aspect ratio of the screen. "
        "0 to automatically base on screen resolution."
        "4:3, 16:9, or 16:10 for those standard ratios. "
        "You can also type in a random fractional number for some custom aspect ratio.",

        [&] (illConsole::ConsoleVariable * var, const char * value) {
            std::istringstream stream(value);
                
            std::string dest;
            if(engine.m_developerConsole->getParamString(stream, dest) 
                    && engine.m_developerConsole->checkParamEnd(stream)) {
                if(dest.compare("4:3") == 0) {
                    engine.m_window->setApsectRatio(illGraphics::ASPECT_4_3);
                }
                else if(dest.compare("16:9") == 0) {
                    engine.m_window->setApsectRatio(illGraphics::ASPECT_16_9);
                }
                else if(dest.compare("16:10") == 0) {
                    engine.m_window->setApsectRatio(illGraphics::ASPECT_16_10);
                }
                else {
                    float floatAspect;
                    std::istringstream floatStream(dest);

                    //TODO: this is kinda shitty, I may need a proper tokenizer for this kind of thing
                    bool floatSuccess = !(floatStream >> floatAspect).fail();
                    floatStream >> dest;

                    if(floatSuccess && floatStream.eof() && floatAspect >= 0.0f) {
                        engine.m_window->setApsectRatio(floatAspect);
                    }
                    else {
                        engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "Expecting either 4:3, 16:9, 16:10, 0, or some arbitrary fractional number greater than 0.");
                        return false;
                    }
                }

                return true;
            }

            return false;
        });

    engine.m_developerConsole->m_variableManager->addVariable("vid_aspect", &cv_vid_aspect);

    //TODO: implement
    illConsole::ConsoleCommand cm_vid_printMonitors(
        "Prints out the names of all detected monitors. "
        "Use the monitor name in the vid_monitor command.",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "I still need to implement this.");
        });

    engine.m_developerConsole->m_commandManager->addCommand("vid_printMonitors", &cm_vid_printMonitors);

    illConsole::ConsoleCommand cm_vid_applyResolution(
        "Applies the screen resolution changes as set in the variables vid_screenWidth, vid_screenHeight, vid_colorDepth, vid_fullScreen.",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            if(engine.m_developerConsole->checkParamEnd(stream)) {
                engine.m_window->resize();
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("vid_applyResolution", &cm_vid_applyResolution);

    illConsole::ConsoleCommand cm_bind("Binds an input to a command name. "
        "A command name may then be put in an input context later. "
        "You can have the same input bound to multiple commands since "
        "you may have multiple contexts when that key can trigger an action. "
        "Example usage bind UP Forward. "
        "You can also bind to a console command. bind F10 toggle vid_showFps"
        "bind <input> <command name or console commands>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string input;
            std::string command;

            if(engine.m_developerConsole->getParamString(stream, input)
                    && engine.m_developerConsole->getParamString(stream, command)) {
                //see if input is a valid input type
                illInput::InputBinding binding = consoleInputToBinding(input.c_str());

                if(binding.m_deviceType == (int) illInput::InputType::INVALID) {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, formatString("Invalid input name %s.", input.c_str()).c_str());
                    return;
                }

                //see if command is a command name
                if(engine.m_developerConsole->m_commandManager->commandExists(command.c_str())) {
                    //ugh...
                    char commandArgs[256];
                    commandArgs[0] = ' ';
                    stream.get(commandArgs + 1, 255);
                    command.append(commandArgs); 

                    //TODO: make the command take a player number
                    engine.m_inputManager->bindAction(0, binding, command.c_str(), illInput::InputManager::ActionType::CONSOLE_COMMAND);
                }
                else {
                    if(engine.m_developerConsole->checkParamEnd(stream)) {
                        engine.m_inputManager->bindAction(0, binding, command.c_str(), illInput::InputManager::ActionType::CONTROL);
                    }
                    else {
                        engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "Unexpected parameters when binding input action name.");
                    }
                }
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("bind", &cm_bind);

    illConsole::ConsoleCommand cm_unbind(
        "Unbinds an input from a command. "
        "unbind <input> <command name or console commands>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            std::istringstream stream(params ? params : "");

            std::string input;

            if(engine.m_developerConsole->getParamString(stream, input)) {
                //see if input is a valid input type
                illInput::InputBinding binding = consoleInputToBinding(input.c_str());

                if(binding.m_deviceType == (int) illInput::InputType::INVALID) {
                    engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, formatString("Invalid input name %s.", input.c_str()).c_str());
                    return;
                }

                //ugh...
                char action[256];
                stream.get(action, 256);

                engine.m_inputManager->unbindAction(0, binding, action);
            }
        });

    engine.m_developerConsole->m_commandManager->addCommand("unbind", &cm_unbind);

    //TODO: implement
    illConsole::ConsoleCommand cm_saveBind(
        "Saves a binding permanently "
        "saveBind <input> <command name or console commands>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "I still need to implement this.");
        });

    engine.m_developerConsole->m_commandManager->addCommand("saveBind", &cm_saveBind);

    //TODO: implement
    illConsole::ConsoleCommand cm_printInputBinds(
        "Prints the input bindings. "
        "You can optionally provide a file to output them to. "
        "printInputBinds <optional file name>",

        [&] (const illConsole::ConsoleCommand *, const char * params) {
            engine.m_developerConsole->printMessage(illLogging::LogDestination::MT_ERROR, "I still need to implement this.");
        });

    engine.m_developerConsole->m_commandManager->addCommand("printInputBinds", &cm_printInputBinds);

    engine.m_developerConsole->consoleInput("../../../config.cfg");

    //init game
    engine.m_window->initialize();

    //disable OS cursor
    glfwSetInputMode(engine.m_window->getGlfwWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    MainController mainController(&engine);
    FixedStepController loopController(&mainController, &engine);
    engine.m_gameController = &loopController;
    
    //main loop, GO!!!!
    loopController.appLoop();    

    //clean up
    glfwTerminate();

    delete engine.m_meshManager;
    delete engine.m_materialManager;
    delete engine.m_textureManager;
    delete engine.m_shaderProgramManager;
    delete engine.m_shaderManager;

    delete engine.m_graphicsBackend;
    delete engine.m_window;
    delete engine.m_inputManager;

    delete illFileSystem::fileSystem;
        
    LOGGER_END_CATCH(illLogging::logger)

    delete engine.m_developerConsole->m_variableManager;
    delete engine.m_developerConsole->m_commandManager;
    delete engine.m_developerConsole;

    delete illLogging::logger;

    return 0;
}



}