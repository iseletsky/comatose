#ifndef COMA_ENTITY_H_
#define COMA_ENTITY_H_

#include "../../../illFramework/Util/RefCountPtr.h"

namespace Coma {
class EntityManager;

class Entity {
public:
    typedef size_t EntityId;

    virtual ~Entity();

    /**
    Code that runs at the beginning of a world update before the physics are updated.
    */
    virtual void beginUpdate(float seconds) {}
    
    /**
    Code that runs during a world update.  Physics are also updated during this step.
    */
    virtual void update(float seconds) {}

    /**
    Code that runs at the end of a world update.
    This is after physics are updated and before the graphics and sound are rendered.
    */
    virtual void endUpdate(float seconds) {}
    
    /**
    Adds this entity to the list of entities whose beginUpdate() method will run during the next world beginUpdate().
    This has to be done every step.
    */
    void addBeginUpdate();

    /**
    Adds this entity to the list of entities whose update() method will run during the next world update().
    */
    void addUpdate();

    /**
    Adds this entity to the list of entities whose endUpdate() method will run during the next world endUpdate().
    */
    void endUpdate();

    inline RefCountPtr<Entity> getRefCountPtr() const {
        return m_sharedPointer;
    }

    inline size_t getEntityId() const {
        return m_entityId;
    }
    
    inline EntityManager * getEntityManager() const {
        return m_entityManager;
    }

protected:
    /**
    Creates the entity.
    */
    Entity(EntityManager * entityManager, bool addBeginUpdate = false, bool addUpdate = false, bool addEndUpdate = false);
    
private:    
    EntityManager * m_entityManager;
    size_t m_entityId;
    RefCountPtr<Entity> m_sharedPointer;

friend EntityManager;
};
}

#endif