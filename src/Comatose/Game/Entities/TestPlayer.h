#ifndef COMA_TEST_PLAYER_H_
#define COMA_TEST_PLAYER_H_

#include "Entity.h"

namespace Coma {
class TestPlayer : public Entity {
public:
    TestPlayer(EntityManager * entityManager)
        : Entity(entityManager, false, true, false)
    {}

    virtual ~TestPlayer() {}

    virtual void update(float seconds) {}

private:
};
}

#endif