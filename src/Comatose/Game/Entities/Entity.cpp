#include "Entity.h"
#include "EntityManager.h"

namespace Coma {
Entity::Entity(EntityManager * entityManager, bool addBeginUpdate, bool addUpdate, bool addEndUpdate) 
    : m_entityManager(entityManager)
{
    m_entityManager->addEntity(this, addBeginUpdate, addUpdate, addEndUpdate);
}

Entity::~Entity() {
    m_entityManager->removeEntity(this);
}

void Entity::addBeginUpdate() {
    m_entityManager->addBeginUpdate(this);
}

void Entity::addUpdate() {
    m_entityManager->addUpdate(this);
}

void Entity::endUpdate() {
    m_entityManager->addEndUpdate(this);
}
}