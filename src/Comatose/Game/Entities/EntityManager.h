#ifndef COMA_ENTITY_MANAGER_H_
#define COMA_ENTITY_MANAGER_H_

#include <vector>

#include "Entity.h"
#include "../../../illFramework/Util/Pool.h"

namespace Coma {
class EntityManager {
public:
    EntityManager();
    ~EntityManager();

    void update(float seconds);

    inline Entity * getEntityForId(size_t entityId) {
        return m_entities.get(entityId);
    }

private:
    enum class UpdateState {
        NOT_UPDATING,
        BEGIN_UPDATE,
        UPDATE,
        END_UPDATE
    } m_updateState;

    void addEntity(Entity * entity, bool beginUpdate, bool update, bool endUpdate);
    void removeEntity(Entity * entity);

    void addBeginUpdate(Entity * entity);
    void addUpdate(Entity * entity);
    void addEndUpdate(Entity * entity);

    Pool<size_t, Entity *> m_entities;

    bool m_currentUpdateList;

    struct UpdateList {
        /*UpdateList()
            : m_beginUpdate(250),
            m_update(250),
            m_endUpdate(250)
        {}*/

        std::vector<RefCountPtr<Entity>> m_beginUpdate;
        std::vector<RefCountPtr<Entity>> m_update;
        std::vector<RefCountPtr<Entity>> m_endUpdate;
    };

    UpdateList m_updateList[2];

friend Entity;
};
}

#endif