#ifndef ILL_STATIC_MESH_H_
#define ILL_STATIC_MESH_H_

#include <glm/glm.hpp>

#include "../Entity.h"
#include "../../../../illFramework/Graphics/Renderer/Frontend/StaticMeshNode.h"

namespace Coma {
class StaticMesh : public Entity {
public:
    StaticMesh(EntityManager * entityManager)
        : Entity(entityManager, false, false, false)
    {}

    virtual ~StaticMesh() {}

private:
    //illRendererCommon::StaticMeshNode 
};
}

#endif