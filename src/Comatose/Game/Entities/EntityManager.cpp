#include "EntityManager.h"

namespace Coma {
EntityManager::EntityManager()
    : m_entities(1000),
    m_currentUpdateList(false),
    m_updateState(UpdateState::NOT_UPDATING)
{
}

EntityManager::~EntityManager() {
}

void EntityManager::update(float seconds) {
    m_updateState = UpdateState::BEGIN_UPDATE;
    
    for(size_t index = 0; index < m_updateList[m_currentUpdateList].m_beginUpdate.size(); ++index) {
        if(!m_updateList[m_currentUpdateList].m_beginUpdate[index].isNull()) {
            m_updateList[m_currentUpdateList].m_beginUpdate[index]->beginUpdate(seconds);
        }
    }

    m_updateList[m_currentUpdateList].m_beginUpdate.clear();
    m_updateState = UpdateState::UPDATE;

    //TODO: Update PhysX world

    for(size_t index = 0; index < m_updateList[m_currentUpdateList].m_update.size(); ++index) {
        if(!m_updateList[m_currentUpdateList].m_update[index].isNull()) {
            m_updateList[m_currentUpdateList].m_update[index]->update(seconds);
        }
    }

    m_updateList[m_currentUpdateList].m_update.clear();
    m_updateState = UpdateState::END_UPDATE;

    for(size_t index = 0; index < m_updateList[m_currentUpdateList].m_endUpdate.size(); ++index) {
        if(!m_updateList[m_currentUpdateList].m_endUpdate[index].isNull()) {
            m_updateList[m_currentUpdateList].m_endUpdate[index]->endUpdate(seconds);
        }
    }
    
    m_updateList[m_currentUpdateList].m_endUpdate.clear();
    m_updateState = UpdateState::NOT_UPDATING;

    m_currentUpdateList = !m_currentUpdateList;
}

void EntityManager::addEntity(Entity * entity, bool beginUpdate, bool update, bool endUpdate) {
    entity->m_entityId = m_entities.getFreeId();

    if(beginUpdate) {
        addBeginUpdate(entity);
    }

    if(update) {
        addUpdate(entity);
    }

    if(endUpdate) {
        addEndUpdate(entity);
    }
}

void EntityManager::removeEntity(Entity * entity) {
    m_entities.remove(entity->m_entityId);
}

void EntityManager::addBeginUpdate(Entity * entity) {
    m_updateList[m_updateState <= UpdateState::BEGIN_UPDATE ? m_currentUpdateList : !m_currentUpdateList].m_beginUpdate.push_back(entity->getRefCountPtr());
}

void EntityManager::addUpdate(Entity * entity) {
    m_updateList[m_updateState <= UpdateState::UPDATE ? m_currentUpdateList : !m_currentUpdateList].m_update.push_back(entity->getRefCountPtr());
}

void EntityManager::addEndUpdate(Entity * entity) {
    m_updateList[m_currentUpdateList].m_endUpdate.push_back(entity->getRefCountPtr());
}

}