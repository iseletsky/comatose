#ifndef COMA_TEST_GROUNDS_CONTROLLER_H_
#define COMA_TEST_GROUNDS_CONTROLLER_H_

#include "../../GameControllerBase.h"
#include "../Entities/EntityManager.h"
#include "../Entities/DebugCamera.h"

namespace illGraphics {
class GraphicsScene;
class RendererBackend;
}

namespace Coma {
struct Engine;

class TestGroundsController : public GameControllerBase {
public:
    TestGroundsController(Engine * engine);

    virtual ~TestGroundsController();

    void update(float seconds);

    void updateSound(float seconds);

    void render();

private:
    Engine * m_engine;
    EntityManager m_entityManager;
    illGraphics::GraphicsScene * m_graphicsScene;
    illGraphics::RendererBackend * m_rendererBackend;

    DebugCamera m_debugCamera;
    size_t m_viewPort;
};
}

#endif