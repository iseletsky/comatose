#include <glm/gtc/type_ptr.hpp>

#include "TestGroundsController.h"
#include "../Entities/TestPlayer.h"
#include "../../Engine.h"

#include "../../../illFramework/Input/InputManager.h"
#include "../../../illFramework/Graphics/Objects/Camera/Camera.h"
#include "../../../illFramework/Graphics/Renderer/Backend/Gl3_3/RendererBackendGl3_3.h"
#include "../../../illFramework/Graphics/Renderer/Frontend/GraphicsScene.h"
#include "../../../illFramework/Graphics/GlfwWindow.h"

#include "../../../illFramework/Graphics/Renderer/Frontend/LightNode.h"
#include "../../../illFramework/Graphics/Renderer/Frontend/StaticMeshNode.h"

//TODO: for debug
#include <fstream>

namespace Coma {
TestGroundsController::TestGroundsController(Engine * engine)
    : m_engine(engine)
{
    /*        
    m_inputContext.bindInput("Cam_ZoomIn", &m_zoomInListener);
    m_inputContext.bindInput("Cam_ZoomOut", &m_zoomOutListener);
    m_inputContext.bindInput("Cam_ZoomDefault", &m_zoomDefaultListener);
    */
    /*m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_X), "HorizontalLook", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_Y), "VerticalLook", illInput::InputManager::ActionType::CONTROL);

    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'A'), "Left", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'S'), "Back", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'D'), "Right", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'W'), "Forward", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'Q'), "RollLeft", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, 'E'), "RollRight", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_SPACE), "Up", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_CONTROL), "Down", illInput::InputManager::ActionType::CONTROL);
    m_engine->m_inputManager->bindAction(0, illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_SHIFT), "Sprint", illInput::InputManager::ActionType::CONTROL);*/

    m_engine->m_inputManager->getInputContextStack(0)->pushInputContext(&m_debugCamera.m_inputContext);
    
    m_rendererBackend = new illGraphics::RendererBackendGl3_3(static_cast<illGraphics::GlBackend *>(engine->m_graphicsBackend));
    m_graphicsScene = new illGraphics::GraphicsScene(m_rendererBackend, engine->m_meshManager, engine->m_materialManager, 
        glm::vec3(38.0f), glm::uvec3(33, 25, 33), 
        glm::vec3(38.0f), glm::uvec3(33, 25, 33));

    m_viewPort = m_graphicsScene->registerViewport();
    m_rendererBackend->initialize(glm::uvec2((unsigned int) m_engine->m_window->getScreenWidth(), (unsigned int) m_engine->m_window->getScreenHeight()), 
        m_engine->m_shaderProgramManager);

    //for now, load the graveyard scene
    {
        m_debugCamera.m_speed = 10.0f;
        m_debugCamera.m_rollSpeed = 50.0f;

        std::ifstream openFile("..\\..\\..\\assets\\maps\\HangarTest.txt");

        //read number of static meshes
        //int numMeshes;
        //openFile >> numMeshes;
                    
        //for now just add these as dynamic meshes, later they will be static meshes
        //for(int mesh = 0; mesh < numMeshes; mesh++) {
        while(!openFile.eof()) {
            //read mesh name
            std::string meshName;
            openFile >> meshName;

            if(openFile.eof()) {
                break;
            }

            enum class ObjectType {
                MESH,
                POINT_LIGHT,
                SPOT_LIGHT,
                VOLUME_LIGHT,
                START
            } objectType = ObjectType::MESH;
                
            if(meshName.compare("__PointLight__") == 0) {
                objectType = ObjectType::POINT_LIGHT;
            }
            else if(meshName.compare("__SpotLight__") == 0) {
                objectType = ObjectType::SPOT_LIGHT;
            }
            else if(meshName.compare("__VolumeLight__") == 0) {
                objectType = ObjectType::VOLUME_LIGHT;
            }
            else if(meshName.compare("__Start__") == 0) {
                objectType = ObjectType::START;
            }

            glm::mat4 transform;

            //read the 3x4 transform                
            for(unsigned int row = 0; row < 3; row++) {
                for(unsigned int column = 0; column < 4; column++) {
                    openFile >> transform[column][row];
                }
            }

            if(objectType == ObjectType::START) {
                m_debugCamera.m_transform = transform;
                continue;
            }
        
            if(objectType == ObjectType::POINT_LIGHT || objectType == ObjectType::SPOT_LIGHT || objectType == ObjectType::VOLUME_LIGHT) {
                Box<> bounds;

                if(objectType == ObjectType::VOLUME_LIGHT) {
                    //read the bounding box
                    for(unsigned int vec = 0; vec < 3; vec++) {
                        openFile >> bounds.m_min[vec];
                    }

                    for(unsigned int vec = 0; vec < 3; vec++) {
                        openFile >> bounds.m_max[vec];
                    }
                }
                    
                //light color
                glm::vec3 color;

                for(unsigned int vec = 0; vec < 3; vec++) {
                    openFile >> color[vec];
                }

                //light intensity
                float intensity;
                openFile >> intensity;

                //whether or not using specular
                bool specular;
                openFile >> specular;

                if(objectType == ObjectType::POINT_LIGHT || objectType == ObjectType::SPOT_LIGHT) {
                    //near attenuation
                    float nearAtten;
                    openFile >> nearAtten;

                    //far attenuation
                    float farAtten;
                    openFile >> farAtten;

                    if(objectType == ObjectType::POINT_LIGHT) {
                        illGraphics::PointLight * lightObj = new illGraphics::PointLight(color, intensity, specular, nearAtten, farAtten);

                        auto newLight = new illGraphics::LightNode(m_graphicsScene, transform, Box<>(glm::vec3(-farAtten), glm::vec3(farAtten)));
                        newLight->m_light = RefCountPtr<illGraphics::LightBase>(lightObj);
                    }
                    else {
                        float coneStart;
                        openFile >> coneStart;

                        float coneEnd;
                        openFile >> coneEnd;

                        illGraphics::SpotLight * lightObj = new illGraphics::SpotLight(color, intensity, specular, nearAtten, farAtten, coneStart, coneEnd);

                        Box<> bounds(glm::vec3(0.0f));

                        //Computing bounding box of light cone
                        //TODO: move this to the util geometry code somewhere
                        {
                            glm::vec3 direction = glm::mat3(transform) * glm::vec3(0.0f, 0.0f, -1.0f);
                            glm::vec3 endPos = direction * farAtten;
                            glm::mediump_float coneDir = glm::degrees(glm::acos(coneEnd));

                            bounds.addPoint(endPos);

                            bounds.addPoint(glm::mat3(glm::rotate(coneDir, glm::vec3(1.0f, 0.0f, 0.0f))) * endPos);
                            bounds.addPoint(glm::mat3(glm::rotate(-coneDir, glm::vec3(1.0f, 0.0f, 0.0f))) * endPos);

                            bounds.addPoint(glm::mat3(glm::rotate(coneDir, glm::vec3(0.0f, 1.0f, 0.0f))) * endPos);
                            bounds.addPoint(glm::mat3(glm::rotate(-coneDir, glm::vec3(0.0f, 1.0f, 0.0f))) * endPos);

                            bounds.addPoint(glm::mat3(glm::rotate(coneDir, glm::vec3(0.0f, 0.0f, 1.0f))) * endPos);
                            bounds.addPoint(glm::mat3(glm::rotate(-coneDir, glm::vec3(0.0f, 0.0f, 1.0f))) * endPos);
                        }

                        auto newLight = new illGraphics::LightNode(m_graphicsScene, transform, bounds);
                        newLight->m_light = RefCountPtr<illGraphics::LightBase>(lightObj);
                    }
                }
                else {
                    bool directional;
                    openFile >> directional;

                    glm::vec3 vector;

                    for(unsigned int vec = 0; vec < 3; vec++) {
                        openFile >> vector[vec];
                    }

                    illGraphics::VolumeLight * lightObj = new illGraphics::VolumeLight(color, intensity, specular, directional, vector);

                    int numPlanes;
                    openFile >> numPlanes;

                    Plane<> lastPlane;
                    glm::mediump_float lastAttenuation;

                    for(int plane = 0; plane < numPlanes; plane++) {
                        openFile >> lastPlane.m_normal.x;
                        openFile >> lastPlane.m_normal.y;
                        openFile >> lastPlane.m_normal.z;
                        openFile >> lastPlane.m_distance;

                        openFile >> lastAttenuation;

                        lightObj->m_planes[plane] = lastPlane;
                        lightObj->m_planeFalloff[plane] = lastAttenuation;
                    }

                    //pad the remaining planes with the previously read plane
                    for(unsigned int plane = numPlanes; plane < illGraphics::MAX_LIGHT_VOLUME_PLANES; plane++) {
                        lightObj->m_planes[plane] = lastPlane;
                        lightObj->m_planeFalloff[plane] = lastAttenuation;
                    }

                    auto newLight = new illGraphics::LightNode(m_graphicsScene, transform, bounds);
                    newLight->m_light = RefCountPtr<illGraphics::LightBase>(lightObj);
                }
            }
            else {
                Box<> bounds;
                    
                //read the bounding box
                for(unsigned int vec = 0; vec < 3; vec++) {
                    openFile >> bounds.m_min[vec];
                }

                for(unsigned int vec = 0; vec < 3; vec++) {
                    openFile >> bounds.m_max[vec];
                }

                //read occluder type
                int occluderType;
                openFile >> occluderType;

                if(occluderType > 2) {
                    LOG_FATAL_ERROR("Invalid occluder type %d for mesh %s", occluderType, meshName.c_str());
                }

                illGraphics::StaticMeshNode * node = new illGraphics::StaticMeshNode(m_graphicsScene,
                    transform, bounds, (illGraphics::StaticMeshNode::OccluderType) occluderType);
                    
                meshName = "meshes/Hangar/" + meshName + ".illmesh";

                node->m_meshId = m_engine->m_meshManager->getIdForName(meshName.c_str());

                //read number of primitive groups
                int numGroups;
                openFile >> numGroups;

                node->m_primitiveGroups.resize(numGroups);

                //read material names
                for(int group = 0; group < numGroups; group++) {
                    std::string matName;
                    openFile >> matName;

                    matName = "meshes/Hangar/" + matName + ".illmat";

                    node->m_primitiveGroups[group].m_materialId = m_engine->m_materialManager->getIdForName(matName.c_str());
                    node->m_primitiveGroups[group].m_visible = true;
                }

                node->load(m_engine->m_meshManager, m_engine->m_materialManager);
            }
        }
    }
}

TestGroundsController::~TestGroundsController() {
    delete m_graphicsScene;
    delete m_rendererBackend;
}

void TestGroundsController::update(float seconds) {
    m_debugCamera.update(seconds);
    m_entityManager.update(seconds);
}

void TestGroundsController::updateSound(float seconds) {
}

void TestGroundsController::render() {
    illGraphics::Camera camera;

    camera.setPerspectiveTransform(m_debugCamera.m_transform, 
        m_engine->m_window->getAspectRatio(), 
        illGraphics::DEFAULT_FOV * m_debugCamera.m_zoom, illGraphics::DEFAULT_NEAR, 2000.0f);

    camera.setViewport(glm::ivec2(0, 0), glm::ivec2(m_engine->m_window->getScreenWidth(), m_engine->m_window->getScreenHeight()));

    m_graphicsScene->setupFrame();
    m_graphicsScene->render(camera, m_viewPort);

    glUseProgram(0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, 0);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, 0);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glShadeModel(GL_SMOOTH);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(glm::value_ptr(camera.getProjection()));

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(glm::value_ptr(camera.getModelView()));

    //debug draw the axes
    glBegin(GL_LINES);
    //x Red
        glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(5.0f, 0.0f, 0.0f);

    //y Green
        glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 5.0f, 0.0f);

    //z Blue
        glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, 5.0f);
    glEnd();
}
}