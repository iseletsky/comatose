#include "MainController.h"
#include "../Engine.h"

#include "TestGrounds/TestGroundsController.h"

namespace Coma {

MainController::MainController(Engine * engine)
    : GameControllerBase(),
    m_engine(engine),
    m_subGame(NULL)
{
    startTestGrounds();
}

void MainController::startTestGrounds() {
    delete m_subGame;
    setSubGame(new TestGroundsController(m_engine));
}

/*void MainController::startMainMenu() {
   m_state = APPST_PREGAME_SPLASH;
   delete m_subGame;
   setSubGame(new MainMenuController(m_engine));
}

void MainController::startSinglePlayer() {
   m_state = APPST_SINGLE_PLAYER;
   delete m_subGame;
   //setSubGame(new SinglePlayer::SinglePlayerGameController(this));
}*/

}