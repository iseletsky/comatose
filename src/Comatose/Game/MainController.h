#ifndef __MAIN_CONTROLLER_H__
#define __MAIN_CONTROLLER_H__

#include <cstdlib>
#include "../GameControllerBase.h"

namespace Coma {

struct Engine;

class MainController : public GameControllerBase {
public:
    MainController(Engine * engine);

    virtual ~MainController() {
        delete m_subGame;
    }

    void update(float seconds) {
        m_subGame->update(seconds);
    }

    void updateSound(float seconds) {
        m_subGame->updateSound(seconds);
    }

    void render() {
        m_subGame->render();
    }
    
    void startTestGrounds();

private:
    inline void setSubGame(GameControllerBase * subGame) {        
        m_subGame = subGame;
        update(1.0f);
    }
        
    Engine * m_engine;
    GameControllerBase * m_subGame;
};

}

#endif