#include <cstring>
#include "../illFramework/Input/inputEnum.h"
#include "inputBinds.h"

illInput::InputBinding consoleInputToBinding(const char * input) {
    //this is most likely an ascii character
    if(strnlen(input, 2) == 1) {        
        if(input[0] >= 33 && input[0] <= 126) {            
            if(input[0] >= 65 && input[0] <= 90) {  //upper case
                return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, input[0]);
            }
            else {                                  //plain ascii
                return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, input[0] - 32);
            }
        }
        else {
            return illInput::InputBinding((int)illInput::InputType::INVALID, 0);
        }
    }

    if(strncmp(input, "SPACE", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_SPACE);
    }

    if(strncmp(input, "BACKSPACE", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_BACKSPACE);
    }

    if(strncmp(input, "TAB", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_TAB);
    }
    
    if(strncmp(input, "RETURN", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_ENTER);
    }

    if(strncmp(input, "ENTER", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_ENTER);
    }

    if(strncmp(input, "PAUSE", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_PAUSE);
    }

    if(strncmp(input, "DELETE", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_DELETE);
    }
    
    if(strncmp(input, "KP_0", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_0);
    }

    if(strncmp(input, "KP_1", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_1);
    }

    if(strncmp(input, "KP_2", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_2);
    }

    if(strncmp(input, "KP_3", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_3);
    }

    if(strncmp(input, "KP_4", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_4);
    }

    if(strncmp(input, "KP_5", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_5);
    }

    if(strncmp(input, "KP_6", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_6);
    }

    if(strncmp(input, "KP_7", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_7);
    }

    if(strncmp(input, "KP_8", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_8);
    }

    if(strncmp(input, "KP_9", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_9);
    }
    
    if(strncmp(input, "KP_PERIOD", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_DECIMAL);
    }

    if(strncmp(input, "KP_DIVIDE", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_DIVIDE);
    }

    if(strncmp(input, "KP_MULTIPLY", 12) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_MULTIPLY);
    }

    if(strncmp(input, "KP_SUBTRACT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_SUBTRACT);
    }

    if(strncmp(input, "KP_ADD", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_ADD);
    }

    if(strncmp(input, "KP_ENTER", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_KP_ENTER);
    }
        
    if(strncmp(input, "UP", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_UP);
    }

    if(strncmp(input, "DOWN", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_DOWN);
    }

    if(strncmp(input, "RIGHT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_RIGHT);
    }

    if(strncmp(input, "LEFT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT);
    }

    if(strncmp(input, "INSERT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_INSERT);
    }

    if(strncmp(input, "HOME", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_HOME);
    }

    if(strncmp(input, "END", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_END);
    }

    if(strncmp(input, "PAGE_UP", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_PAGE_UP);
    }

    if(strncmp(input, "PAGE_DOWN", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_PAGE_DOWN);
    }

    if(strncmp(input, "F1", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F1);
    }

    if(strncmp(input, "F2", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F2);
    }

    if(strncmp(input, "F3", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F3);
    }

    if(strncmp(input, "F4", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F4);
    }

    if(strncmp(input, "F5", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F5);
    }

    if(strncmp(input, "F6", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F6);
    }

    if(strncmp(input, "F7", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F7);
    }

    if(strncmp(input, "F8", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F8);
    }

    if(strncmp(input, "F9", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F9);
    }

    if(strncmp(input, "F10", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F10);
    }

    if(strncmp(input, "F11", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F11);
    }

    if(strncmp(input, "F12", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F12);
    }

    if(strncmp(input, "F13", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F13);
    }

    if(strncmp(input, "F14", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F14);
    }

    if(strncmp(input, "F15", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F15);
    }

    if(strncmp(input, "F16", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F16);
    }

    if(strncmp(input, "F17", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F17);
    }

    if(strncmp(input, "F18", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F18);
    }

    if(strncmp(input, "F19", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F19);
    }

    if(strncmp(input, "F20", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F20);
    }

    if(strncmp(input, "F21", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F21);
    }

    if(strncmp(input, "F22", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F22);
    }

    if(strncmp(input, "F23", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F23);
    }

    if(strncmp(input, "F24", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F24);
    }

    if(strncmp(input, "F25", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_F25);
    }

    if(strncmp(input, "NUM_LOCK", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_NUM_LOCK);
    }

    if(strncmp(input, "CAPS_LOCK", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_CAPS_LOCK);
    }

    if(strncmp(input, "SCROLL_LOCK", 15) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_SCROLL_LOCK);
    }

    if(strncmp(input, "RIGHT_SHIFT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_RIGHT_SHIFT);
    }

    if(strncmp(input, "LEFT_SHIFT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_SHIFT);
    }

    if(strncmp(input, "RRIGHT_CONTROL", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_RIGHT_CONTROL);
    }

    if(strncmp(input, "LEFT_CONTROL", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_CONTROL);
    }

    if(strncmp(input, "RIGHT_ALT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_RIGHT_ALT);
    }

    if(strncmp(input, "LEFT_ALT", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_ALT);
    }

    if(strncmp(input, "RIGHT_SUPER", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_RIGHT_SUPER);
    }

    if(strncmp(input, "LEFT_SUPER", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_LEFT_SUPER);
    }

    if(strncmp(input, "MENU", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_MENU);
    }

    if(strncmp(input, "PRINT_SCREEN", 15) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_PRINT_SCREEN);
    }

    if(strncmp(input, "ESC", 10) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_KEYBOARD, GLFW_KEY_ESCAPE);
    }

    //TODO: more keyboard stuff to come
    
    if(strncmp(input, "MOUSE_WHEEL_UP", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_Y_POS);
    }

    if(strncmp(input, "MOUSE_WHEEL_DOWN", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_Y_NEG);
    }

    if(strncmp(input, "MOUSE_WHEEL_VERT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_Y);
    }

    if(strncmp(input, "MOUSE_WHEEL_LEFT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_X_NEG);
    }

    if(strncmp(input, "MOUSE_WHEEL_RIGHT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_X_POS);
    }

    if(strncmp(input, "MOUSE_WHEEL_HORZ", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_WHEEL, (int)illInput::Axis::AX_X);
    }

    if(strncmp(input, "MOUSE_MOVE_UP", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_Y_POS);
    }

    if(strncmp(input, "MOUSE_MOVE_DOWN", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_Y_NEG);
    }

    if(strncmp(input, "MOUSE_MOVE_LEFT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_X_NEG);
    }

    if(strncmp(input, "MOUSE_MOVE_RIGHT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_X_POS);
    }

    if(strncmp(input, "MOUSE_MOVE_VERT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_Y);
    }

    if(strncmp(input, "MOUSE_MOVE_HORZ", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE, (int)illInput::Axis::AX_X);
    }

    if(strncmp(input, "MOUSE_LEFT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_BUTTON, 1);
    }

    if(strncmp(input, "MOUSE_RIGHT", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_BUTTON, 3);
    }

    if(strncmp(input, "MOUSE_MIDDLE", 20) == 0) {
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_BUTTON, 2);
    }

    //a mouse button
    if(strncmp(input, "MOUSE_", 8) == 1) {
        int button = atoi(input + 6);
        return illInput::InputBinding((int)illInput::InputType::PC_MOUSE_BUTTON, button);
    }

    return illInput::InputBinding((int)illInput::InputType::INVALID, 0);
}

std::string inputBindingToConsoleInput(const illInput::InputBinding& binding) {
    return "TODO";  //TODO
}