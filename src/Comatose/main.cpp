#include "../illFramework/Util/osDef.h"
#include "comatose.h"

#if CURR_OS == OS_WIN

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR szCmdLine, int sw)
{
    //TODO: parse windows command line
    const char * argv0 = "Comatose.exe";

    return Coma::comaMain(1, &argv0);
}

#else

int main(int argc, const char ** argv) {
    return Coma::comaMain(0, NULL);
}

#endif