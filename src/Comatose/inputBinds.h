#ifndef ILL_INPUT_BINDS_H_
#define ILL_INPUT_BINDS_H_

#include <string>

#include "../illFramework/Input/InputBinding.h"

illInput::InputBinding consoleInputToBinding(const char * input);
std::string inputBindingToConsoleInput(const illInput::InputBinding& binding);

#endif