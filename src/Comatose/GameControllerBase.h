#ifndef COMA_GAME_CONTROLLER_BASE_H_
#define COMA_GAME_CONTROLLER_BASE_H_

namespace Coma {

class GameControllerBase
{
public:
    virtual ~GameControllerBase() {}

    virtual void update(float seconds) = 0;
    virtual void updateSound(float seconds) = 0;
    virtual void render() = 0;

protected:
    GameControllerBase() {}
};

}

#endif