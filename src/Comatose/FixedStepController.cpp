#include <stdint.h>
#include <chrono>
#include <thread>

#include "FixedStepController.h"
#include "GameControllerBase.h"

#include "Engine.h"
#include "../illFramework/Graphics/API/GraphicsBackend.h"
#include "../illFramework/Graphics/GlfwWindow.h"
#include "../illFramework/Logging/logging.h"

namespace Coma {

const float STEP_SIZE = 1.0f / 60.0f;     //makes application run at a base FPS of 60
const int MAX_STEPS = 10;

FixedStepController::FixedStepController(GameControllerBase * gameController, Engine * engine) 
    : m_state(State::APPST_INITIALIZED),
    m_gameController(gameController),
    m_engine(engine)
{}

void FixedStepController::appLoop() {      
    //if the application controller isn't in the expected state error
    if (m_state != State::APPST_INITIALIZED) {
        LOG_FATAL_ERROR("Application in unexpected state.");
    }

    m_state = State::APPST_RUNNING;

    float timeAccumulator = 0.0f;
    std::chrono::steady_clock::time_point lastLoopTime = std::chrono::steady_clock::now();

    while (m_state == State::APPST_RUNNING) {
        //poll input events from the game's window
        glfwPollEvents();        
        if(glfwWindowShouldClose(m_engine->m_window->getGlfwWindow())) {
            m_state = State::APPST_EXITING;
            break;
        }
        
        //compute time since last game loop
        std::chrono::steady_clock::time_point currentLoopTime = std::chrono::steady_clock::now();
        float seconds = (float) (std::chrono::duration_cast<std::chrono::milliseconds>(currentLoopTime - lastLoopTime).count()
             * std::chrono::milliseconds::period::num) / (float) (std::chrono::milliseconds::period::den);
        lastLoopTime = currentLoopTime;
        timeAccumulator += seconds;
        
        int steps = 0;
                
        //run the game loop with a fixed step
        while(timeAccumulator > STEP_SIZE && steps++ < MAX_STEPS) {
            timeAccumulator -= STEP_SIZE;

            m_gameController->update(STEP_SIZE);
        }
        
        /////////////////////
        //update sound
        m_gameController->updateSound(seconds);

        /////////////////////
        //draw screen
        m_engine->m_window->beginFrame();
        m_engine->m_graphicsBackend->beginFrame();
                
        m_gameController->render();
        
        m_engine->m_graphicsBackend->endFrame();
        m_engine->m_window->endFrame();

        //force delay 1 ms to avoid precision issues
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_state = State::APPST_INITIALIZED;
}

}
