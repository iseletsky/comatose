#ifndef COMA_ENGINE_H_
#define COMA_ENGINE_H_

#include <stdint.h>

namespace illConsole {
class DeveloperConsole;
}

template<typename T, typename Loader> class ResourceManager;
template<typename T, typename Loader> class NamedResourceManager;

namespace illGraphics {
class GlfwWindow;
class GraphicsBackend;

class Material;
struct MaterialLoader;
typedef NamedResourceManager<Material, MaterialLoader> MaterialManager;

class Shader;
typedef ResourceManager<Shader, GraphicsBackend> ShaderManager;

class ShaderProgram;
struct ShaderProgramLoader;
typedef ResourceManager<ShaderProgram, ShaderProgramLoader> ShaderProgramManager;

class Texture;
typedef NamedResourceManager<Texture, GraphicsBackend> TextureManager;

class AnimSet;
typedef NamedResourceManager<AnimSet, GraphicsBackend> AnimSetManager;

class Mesh;
typedef NamedResourceManager<Mesh, GraphicsBackend> MeshManager;

class Skeleton;
typedef NamedResourceManager<Skeleton, GraphicsBackend> SkeletonManager;

class SkeletonAnimation;
typedef NamedResourceManager<SkeletonAnimation, GraphicsBackend> SkeletonAnimationManager;
}

namespace illInput {
class InputManager;
}

namespace Coma {
class FixedStepController;

struct Engine {
    //TODO: think of a cleverer way to protect these with getters, or not...  What idiot programmer would ever use these unsafely anyway?  (hint, hint... don't try to change these, just use them)
    //just don't mess with these, do you really want me to go in and write getters when I can just leave them public?
    //I have to set them from main() somehow anyway and I'd have to do PIMPL or make the constructor take them or something.  meh...
    illConsole::DeveloperConsole * m_developerConsole;

    illGraphics::GlfwWindow * m_window;
    illGraphics::GraphicsBackend * m_graphicsBackend;

    illGraphics::MaterialManager * m_materialManager;
    illGraphics::ShaderManager * m_shaderManager;
    illGraphics::ShaderProgramManager * m_shaderProgramManager;
    illGraphics::TextureManager * m_textureManager;
    
    illGraphics::AnimSetManager * m_animSetManager;
    illGraphics::MeshManager * m_meshManager;
    illGraphics::SkeletonManager * m_skeletonManager;
    illGraphics::SkeletonAnimationManager * m_skeletonAnimationManager;

    illInput::InputManager * m_inputManager;

    FixedStepController * m_gameController;
}; 
}

#endif