#ifndef COMA_FIXED_STEP_CONTROLLER_H_
#define COMA_FIXED_STEP_CONTROLLER_H_

namespace Coma {
class GameControllerBase;
struct Engine;

class FixedStepController {
public:
    FixedStepController(GameControllerBase * gameController, Engine * engine);
    ~FixedStepController() {}

    void appLoop();

    inline void exitApp() {
        m_state = State::APPST_EXITING;
    }
    
private:
    enum class State {
        APPST_INITIALIZED,
        APPST_RUNNING,
        APPST_EXITING
    };
    
    State m_state;
    GameControllerBase * m_gameController;
    Engine * m_engine;
};

}

#endif
